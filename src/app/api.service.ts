import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  
   //base_url = 'http://localhost/ejob/admin/api/';
   base_url = 'https://ejob.mytasks.co.in/admin/api/';

  constructor(private httpClient: HttpClient) { 

  }

  public getItems(){
    return this.httpClient.get(this.base_url+'getItems');
  }

  public getCurrencies(){
    return this.httpClient.get(this.base_url+'getCurrencies');
  }


  public getCities(){
    return this.httpClient.get(this.base_url+'getCities');
  }

  public getEmployerLetterTypes(){
    return this.httpClient.get(this.base_url+'getEmailLetterTypes');
  }

   public getIndustries(){
    return this.httpClient.get(this.base_url+'getIndustries');
  }

  public getJobseekers(){
    return this.httpClient.get(this.base_url+'getUsers/jobseeker/Yes');
  }

  public getProfilesByIndustries(industryid){
    return this.httpClient.get(this.base_url+'getProfilesByIndustries/'+industryid);
  }

  public getProfiles(){
    return this.httpClient.get(this.base_url+'getProfiles');
  }

  public getDesignations(){
    return this.httpClient.get(this.base_url+'getDesignations');
  }

  public getQualifications(){
    return this.httpClient.get(this.base_url+'getQualifications');
  }

  public getSpecializationsByQualification(qualificationid){
    return this.httpClient.get(this.base_url+'getSpecializationsByQualification/'+qualificationid);
  }

  public getCountries(){
    return this.httpClient.get(this.base_url+'getCountries');
  }


  public getCastCategories(){
    return this.httpClient.get(this.base_url+'getCastCategories');
  }

  public getStatesByCountry(countryid){
    return this.httpClient.get(this.base_url+'getStatesByCountry/'+countryid);
  }

   public getStatesByCountries(countries){
   const formData1 = new FormData();
   formData1.append('countries', countries);
   return this.httpClient.post(this.base_url+'getStatesByCountries',formData1);
  }

   public getCitiesByState(stateid){
    return this.httpClient.get(this.base_url+'getCitiesByState/'+stateid);
  }

  public getCitiesByStates(states){
    const formData1 = new FormData();
    formData1.append('states', states);
    return this.httpClient.post(this.base_url+'getCitiesByStates',formData1);
  }

  public getUserInfo(userid){
    return this.httpClient.get(this.base_url+'getUserInfo/'+userid);
  }

   public inbox(userid){
    return this.httpClient.get(this.base_url+'inbox/'+userid);
  }

   public outbox(userid){
    return this.httpClient.get(this.base_url+'outbox/'+userid);
  }

   public mail(id){
    return this.httpClient.get(this.base_url+'mail/'+id);
  }

  public getSettings(userid){
    return this.httpClient.get(this.base_url+'getSettings/'+userid);
  }

  public sendVerifyEmail(userid){
    return this.httpClient.get(this.base_url+'sendVerifyEmail/'+userid);
  }

  public verifyEmail(code){
    return this.httpClient.get(this.base_url+'verifyEmail/'+code);
  }

  public getBlockedCompanies(userid){
    return this.httpClient.get(this.base_url+'getBlockedCompanies/'+userid);
  }

  public getResumes(){
    return this.httpClient.get(this.base_url+'getResumes');
  }

  public getSavedResumes(userid,deleted){
    return this.httpClient.get(this.base_url+'getSavedResumes/'+userid+'/'+deleted);
  }

  public deleteSavedResume(userid,id){
    return this.httpClient.get(this.base_url+'deleteSavedResume/'+userid+'/'+id);
  }

  public getContactPersons(userid){
    return this.httpClient.get(this.base_url+'getContactPersons/'+userid);
  }

  public getEmployerLetters(userid){
    return this.httpClient.get(this.base_url+'getEmployerLetters/'+userid);
  }


   public getContactsByIds(contacts){
   const formData1 = new FormData();
   formData1.append('contacts', contacts);
   return this.httpClient.post(this.base_url+'getContactsByIds',formData1);
  }


  public getExperiences(userid){
    return this.httpClient.get(this.base_url+'getExperiences/'+userid);
  }

  public getPreferrences(userid){
    return this.httpClient.get(this.base_url+'getPreferrences/'+userid);
  }


  public getLanguages(userid){
    return this.httpClient.get(this.base_url+'getLanguages/'+userid);
  }

   public getAllEducation(userid){
    return this.httpClient.get(this.base_url+'getAllEducation/'+userid);
  }


   public GetMailerSettings(){
    return this.httpClient.get(this.base_url+'GetMailerSettings');
  }

   public postContact(formData){


   const formData1 = new FormData();

   formData1.append('name', formData.name);
   formData1.append('email', formData.email);
   formData1.append('subject', formData.subject);
   formData1.append('message', formData.message);
   formData1.append('phone', formData.phone);

           // let headers = new Headers({ 'Content-Type': 'application/json' });

          //  headers.append('Access-Control-Allow-Origin','*');

          //  let options = new RequestOptions({ headers:headers });

    return this.httpClient.post(this.base_url+'postContact',formData1);
  }

  public postContact1(formData){


   const formData1 = new FormData();

   formData1.append('email', formData.email);

   // let headers = new Headers({ 'Content-Type': 'application/json' });

  //  headers.append('Access-Control-Allow-Origin','*');

  //  let options = new RequestOptions({ headers:headers });

    return this.httpClient.post(this.base_url+'addSubscriber',formData1);
  }

  public register(formData){


   const formData1 = new FormData();

   formData1.append('name', formData.name);
   formData1.append('email', formData.email);
   formData1.append('mobile', formData.mobile);
   formData1.append('password', formData.password);
   formData1.append('usertype', formData.usertype);
    formData1.append('settings', formData.settings);

    return this.httpClient.post(this.base_url+'register',formData1);
  }

   public updateSetting(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
    formData1.append('settings', formData.settings);

    return this.httpClient.post(this.base_url+'updateSetting',formData1);
  }

   public updateBlockedCompanies(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
    formData1.append('settings', formData.settings);

    return this.httpClient.post(this.base_url+'updateBlockedCompanies',formData1);
  }


  postFile(fileToUpload: File) {

    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this.httpClient.post(this.base_url+'postFile',formData);
  
 }

   public updateEmployerProfile(formData,fileToUpload: File){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   if(fileToUpload !== null)
   {
    formData1.append('fileKey', fileToUpload, fileToUpload.name);
   }
   formData1.append('name', formData.name);
   formData1.append('companyname', formData.companyname);
   formData1.append('companytype', formData.companytype);
   formData1.append('companywebsite', formData.companywebsite);
   formData1.append('contact', formData.contact);
   formData1.append('empcount', formData.empcount);
   formData1.append('faxno', formData.fax);
   formData1.append('panno', formData.panno);
   formData1.append('tinno', formData.tinno);
   formData1.append('address', formData.address);
   formData1.append('industry', formData.industry);
   formData1.append('industryid', formData.industryid);
   formData1.append('country', formData.country);
   formData1.append('countryid', formData.countryid);
   formData1.append('state', formData.state);
   formData1.append('stateid', formData.stateid);
   formData1.append('city', formData.city);
   formData1.append('cityid', formData.cityid);
   formData1.append('logo', formData.logo);

    return this.httpClient.post(this.base_url+'updateEmployerProfile',formData1);
  }

   public saveContactPerson(formData){


   const formData1 = new FormData();

   formData1.append('companyid', formData.companyid);
   formData1.append('id', formData.id);
   formData1.append('firstname', formData.firstname);
   formData1.append('lastname', formData.lastname);
   formData1.append('email', formData.email);
   formData1.append('contact', formData.contact);
   formData1.append('mobile', formData.mobile);
   formData1.append('designation', formData.designation);
   formData1.append('designationid', formData.designationid);

    return this.httpClient.post(this.base_url+'saveContactPerson',formData1);
  }

  public deleteContactPerson(id,companyid){
    return this.httpClient.get(this.base_url+'deleteContactPerson/'+id+'/'+companyid);
  }

  public editContactPerson(id){
    return this.httpClient.get(this.base_url+'getContactPerson/'+id);
  }

    public saveEmployerLetter(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('id', formData.id);
   formData1.append('lettertype', formData.lettertype);
   formData1.append('from', formData.from);
   formData1.append('purpose', formData.purpose);
   formData1.append('body', formData.body);
   formData1.append('subject', formData.subject);

    return this.httpClient.post(this.base_url+'saveEmployerLetter',formData1);
  }

  public sendEmail(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('from', formData.from);
   formData1.append('jobseekers', formData.jobseekers);
   formData1.append('body', formData.body);
   formData1.append('subject', formData.subject);

    return this.httpClient.post(this.base_url+'sendEmail',formData1);
  }


  public deleteEmployerLetter(id,userid){
    return this.httpClient.get(this.base_url+'deleteEmployerLetter/'+id+'/'+userid);
  }

  public editEmployerLetter(id){
    return this.httpClient.get(this.base_url+'getEmployerLetter/'+id);
  }

  public updateProfile(formData,fileToUpload: File,fileToUpload1: File){

   var d = new Date(formData.dob);
    var day = d.getDate();
    var month =  d.getMonth()+1;
    var year = d.getFullYear();
    var date = year+'-'+month+'-'+day;

   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   if(fileToUpload !== null)
   {
    formData1.append('fileKey', fileToUpload, fileToUpload.name);
   }
    if(fileToUpload1 !== null)
   {
    formData1.append('fileKey1', fileToUpload1, fileToUpload1.name);
   }
   formData1.append('name', formData.name);
   formData1.append('address', formData.address);
   formData1.append('industry', formData.industry);
   formData1.append('industryid', formData.industryid);
   formData1.append('country', formData.country);
   formData1.append('countryid', formData.countryid);
   formData1.append('state', formData.state);
   formData1.append('stateid', formData.stateid);
   formData1.append('city', formData.city);
   formData1.append('cityid', formData.cityid);
   formData1.append('logo', formData.logo);
   formData1.append('resume', formData.resume);
   formData1.append('profile', formData.profile);
   formData1.append('profileid', formData.profileid);
   formData1.append('designation', formData.designation);
   formData1.append('designationid', formData.designationid);
   formData1.append('caddress', formData.caddress);
   formData1.append('address', formData.address);
   formData1.append('expyears', formData.expyears);
   formData1.append('expmonths', formData.expmonths);
   formData1.append('ctc', formData.ctc);
   formData1.append('currency', formData.currency);
   formData1.append('currentlocation', formData.currentlocation);
   formData1.append('outsideindia', formData.outsideindia);
   formData1.append('skill', formData.skill);
   formData1.append('dob', date);
   formData1.append('gender', formData.gender);
   formData1.append('landline', formData.landline);
   formData1.append('maritalstatus', formData.maritalstatus);
   formData1.append('pincode', formData.pincode);
   formData1.append('linkedin', formData.linkedin);
   formData1.append('email', formData.email);
   formData1.append('mobile', formData.mobile);

    return this.httpClient.post(this.base_url+'updateProfile',formData1);
  }

   public updateOther(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('passport', formData.passport);
   formData1.append('license', formData.license);
   formData1.append('category', formData.category);
   formData1.append('categoryid', formData.categoryid);
   formData1.append('physicallychallenged', formData.physicallychallenged);
   formData1.append('description', formData.description);
   formData1.append('expectedsalary', formData.expectedsalary);

    return this.httpClient.post(this.base_url+'updateOther',formData1);
  }



  public updatePassword(formData){


   const formData1 = new FormData();
   formData1.append('oldpassword', formData.oldpassword);
   formData1.append('password', formData.password);
   formData1.append('userid', formData.userid);

    return this.httpClient.post(this.base_url+'changePassword',formData1);
  }

  public forgotPassword(formData){


   const formData1 = new FormData();

   formData1.append('email', formData.email);

    return this.httpClient.post(this.base_url+'forgotPassword',formData1);
  }

    public resetPassword(formData){


   const formData1 = new FormData();

   formData1.append('code', formData.code);
   formData1.append('email', formData.email);
    formData1.append('password', formData.password);

    return this.httpClient.post(this.base_url+'resetPassword',formData1);
  }


    public login(formData){


   const formData1 = new FormData();

   formData1.append('email', formData.email);
   formData1.append('password', formData.password);
   formData1.append('usertype', formData.usertype);

    return this.httpClient.post(this.base_url+'login',formData1);
  }

  public loginWithLinkedIn(code){


     const formData1 = new FormData();

     formData1.append('code', code);

     return this.httpClient.post(this.base_url+'loginWithLinkedIn',formData1);
  }

  public loginWithGoogle(formData){


   const formData1 = new FormData();

   formData1.append('email', formData.email);
   formData1.append('name', formData.name);
   formData1.append('id', formData.id);
   formData1.append('image', formData.photoUrl);

    return this.httpClient.post(this.base_url+'loginWithGoogle',formData1);
  }

  public loginWithFacebook(formData){


   const formData1 = new FormData();

   formData1.append('email', formData.email);
   formData1.append('name', formData.name);
   formData1.append('id', formData.id);
   formData1.append('image', formData.photoUrl);

    return this.httpClient.post(this.base_url+'loginWithFacebook',formData1);
  }

  public validateMobileno(formData){


   const formData1 = new FormData();

   formData1.append('mobile', formData.mobile);
   formData1.append('type', formData.type);

    return this.httpClient.post(this.base_url+'validateMobileno',formData1);
  }

  public saveAddress(formData){

   const formData1 = new FormData();

   formData1.append('name', formData.name);
   formData1.append('requestid', formData.requestid);
   formData1.append('userid', formData.userid);
   formData1.append('newuser', formData.newuser);
   formData1.append('contact', formData.contact);
   formData1.append('city', formData.city);
   formData1.append('locality', formData.locality);
   formData1.append('landmark', formData.landmark);
   formData1.append('address', formData.address);
   formData1.append('type', formData.type);
   formData1.append('cityid', formData.cityid);
   formData1.append('localityid', formData.localityid);

    return this.httpClient.post(this.base_url+'saveAddress',formData1);
  }

  public saveExperience(formData){

   var d = new Date(formData.fromyear);
    var day = d.getDate();
    var month =  d.getMonth()+1;
    var year = d.getFullYear();
    var date1 = year+'-'+month+'-'+day;

     var d = new Date(formData.toyear);
    var day = d.getDate();
    var month =  d.getMonth()+1;
    var year = d.getFullYear();
    var date2 = year+'-'+month+'-'+day;


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('id', formData.id);
   formData1.append('companyname', formData.companyname);
   formData1.append('employertype', formData.employertype);
   formData1.append('fromyear', date1);
   formData1.append('toyear', date2);
   formData1.append('profile', formData.profile);
   formData1.append('designation', formData.designation);
   formData1.append('noticeperiod', formData.noticeperiod);

    return this.httpClient.post(this.base_url+'saveExperience',formData1);
  }

  public deleteExperience(id,userid){
    return this.httpClient.get(this.base_url+'deleteExperience/'+id+'/'+userid);
  }

  public editExperience(id){
    return this.httpClient.get(this.base_url+'getExperience/'+id);
  }

   public saveEducation(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('id', formData.id);
   formData1.append('qualification', formData.qualification);
   formData1.append('qualificationid', formData.qualificationid);
   formData1.append('specialization', formData.specialization);
   formData1.append('specializationid', formData.specializationid);
   formData1.append('university', formData.university);
   formData1.append('coursetype', formData.coursetype);
   formData1.append('institute', formData.institute);
   formData1.append('yearofpassing', formData.yearofpassing);
   formData1.append('marks', formData.marks);
   formData1.append('grade', formData.grade);

    return this.httpClient.post(this.base_url+'saveEducation',formData1);
  }

  public deleteEducation(id,userid){
    return this.httpClient.get(this.base_url+'deleteEducation/'+id+'/'+userid);
  }

  public editEducation(id){
    return this.httpClient.get(this.base_url+'getEducation/'+id);
  }

    public savePreferrence(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('id', formData.id);
   formData1.append('countryid', formData.countryid);
   formData1.append('stateid', formData.stateid);
   formData1.append('country', formData.country);
   formData1.append('state', formData.state);
   formData1.append('cityid', formData.cityid);
   formData1.append('city', formData.city);

    return this.httpClient.post(this.base_url+'savePreferrence',formData1);
  }

  public deletePreferrence(id,userid){
    return this.httpClient.get(this.base_url+'deletePreferrence/'+id+'/'+userid);
  }

  public editPreferrence(id){
    return this.httpClient.get(this.base_url+'getPreferrence/'+id);
  }

    public saveLanguage(formData){


   const formData1 = new FormData();

   formData1.append('userid', formData.userid);
   formData1.append('id', formData.id);
   formData1.append('language', formData.language);
   formData1.append('level', formData.level);
   formData1.append('read', formData.read);
   formData1.append('write', formData.write);
   formData1.append('speak', formData.speak);

    return this.httpClient.post(this.base_url+'saveLanguage',formData1);
  }

  public deleteLanguage(id,userid){
    return this.httpClient.get(this.base_url+'deleteLanguage/'+id+'/'+userid);
  }

  public editLanguage(id){
    return this.httpClient.get(this.base_url+'getLanguage/'+id);
  }


  public postJob(formData){

    var d = new Date(formData.expdate);
    var day = d.getDate();
    var month =  d.getMonth()+1;
    var year = d.getFullYear();
    var date = year+'-'+month+'-'+day;
    

   const formData1 = new FormData();
   formData1.append('id', formData.id);
   formData1.append('userid', formData.userid);
   formData1.append('jobtype', formData.jobtype);
   formData1.append('title', formData.title);
   formData1.append('keywords', formData.keywords);
   formData1.append('vacancies', formData.vacancies);
   formData1.append('candidature', formData.candidature);
   formData1.append('description', formData.description);
   formData1.append('minexp', formData.minexp);
   formData1.append('maxexp', formData.maxexp);
   formData1.append('currency', formData.currency);
   formData1.append('minctc', formData.minctc);
   formData1.append('maxctc', formData.maxctc);
   formData1.append('ctcstatus', formData.ctcstatus);
   formData1.append('countries', formData.countries);
   formData1.append('states', formData.states);
   formData1.append('cities', formData.cities);
   formData1.append('industries', formData.industries);
   formData1.append('profiles', formData.profiles);
   formData1.append('designations', formData.designations);
   formData1.append('expdate', date);
   formData1.append('condition', formData.condition);
   formData1.append('ugqualifications', formData.ugqualifications);
   formData1.append('pgqualifications', formData.pgqualifications);
   formData1.append('doctoratequalifications', formData.doctoratequalifications);
   formData1.append('contacts', formData.contacts);
   formData1.append('workfromhome', formData.workfromhome);
   formData1.append('billingcycle', formData.billingcycle);
   formData1.append('lakhs', formData.lakhs);
   formData1.append('thousands', formData.thousands);
   formData1.append('joiningmonth', formData.joiningmonth);
   formData1.append('contractperiod', formData.contractperiod);

    return this.httpClient.post(this.base_url+'postJob',formData1);
  }

   public getEmployerJobs(id,status){
    return this.httpClient.get(this.base_url+'getEmployerJobs/'+id+'/'+status);
  }

   public deleteJob(id,userid,status){
   const formData1 = new FormData();
   formData1.append('id', id);
   formData1.append('userid', userid);
   formData1.append('status', status);
    return this.httpClient.post(this.base_url+'deleteJob',formData1);
  }

  public getJobDetail(id){
    return this.httpClient.get(this.base_url+'getJobDetail/'+id);
  }

  public getJobContacts(id){
    return this.httpClient.get(this.base_url+'getJobContacts/'+id);
  }

  public getContent(){
    return this.httpClient.get(this.base_url+'getContent');
  }

  public getSocialLinks(){
    return this.httpClient.get(this.base_url+'getSocialLinks');
  }

  public getJobLocations(id){
    return this.httpClient.get(this.base_url+'getJobLocations/'+id);
  }

  public getJobQualifications(id){
    return this.httpClient.get(this.base_url+'getJobQualifications/'+id);
  }

   public getJobRoles(id){
    return this.httpClient.get(this.base_url+'getJobRoles/'+id);
  }

  public getAllJobs(){
    return this.httpClient.get(this.base_url+'getAllJobs');
  }

  public getGovernmentJobs(){
    return this.httpClient.get(this.base_url+'getGovernmentJobs');
  }

  public getFeaturedJobs(){
    return this.httpClient.get(this.base_url+'getFeaturedJobs');
  }

   public getFeaturedGovernmentJobs(){
    return this.httpClient.get(this.base_url+'getFeaturedGovernmentJobs');
  }

   public getJobProfiles(){
    return this.httpClient.get(this.base_url+'getJobProfiles');
  }

  public getJobsByProfile(id){
    return this.httpClient.get(this.base_url+'getJobsByProfile/'+id);
  }

  public getJobsByLocation(id){
    return this.httpClient.get(this.base_url+'getJobsByLocation/'+id);
  }

   public getJobsByEmployer(id){
    return this.httpClient.get(this.base_url+'getJobsByEmployer/'+id);
  }

   public getJobsByDate(id){
    return this.httpClient.get(this.base_url+'getJobsByDate/'+id);
  }

  public filterJobs(formData){


   const formData1 = new FormData();
   formData1.append('countries', formData.countries);
   formData1.append('profiles', formData.profiles);
   formData1.append('skill', formData.skill);
   formData1.append('experience', formData.experience);
   formData1.append('ctc', formData.ctc);

    return this.httpClient.post(this.base_url+'filterJobs',formData1);
  }

    public filterResumes(formData){


   const formData1 = new FormData();
   formData1.append('countries', formData.countries);
   formData1.append('profiles', formData.profiles);
   formData1.append('industries', formData.industries);
   formData1.append('designations', formData.designations);
   formData1.append('name', formData.name);
   formData1.append('experience', formData.experience);
   formData1.append('gender', formData.gender);
   formData1.append('keyword', formData.keyword);
   formData1.append('agefrom', formData.agefrom);
   formData1.append('ageto', formData.ageto);

    return this.httpClient.post(this.base_url+'filterResumes',formData1);
  }

  public filterEmployerJobs(formData){


   const formData1 = new FormData();
   formData1.append('countries', formData.countries);
   formData1.append('industries', formData.industries);
   formData1.append('skill', formData.skill);
   formData1.append('experience', formData.experience);
   formData1.append('title', formData.title);
   formData1.append('userid', formData.userid);
   formData1.append('approved', formData.approved);

    return this.httpClient.post(this.base_url+'filterEmployerJobs',formData1);
  }

  public getBlogs(){
    return this.httpClient.get(this.base_url+'getBlogs');
  }

  public getBlog(id){
    return this.httpClient.get(this.base_url+'getBlog/'+id);
  }

  public getInterviewers(){
    return this.httpClient.get(this.base_url+'getInterviewers');
  }

  public getInterviewer(id){
    return this.httpClient.get(this.base_url+'getInterviewer/'+id);
  }


  public saveJob(id,userid){
   const formData1 = new FormData();
   formData1.append('id', id);
   formData1.append('userid', userid);
   return this.httpClient.post(this.base_url+'saveJob',formData1);
  }

  public applyForJob(id,userid){
   const formData1 = new FormData();
   formData1.append('id', id);
   formData1.append('userid', userid);
   return this.httpClient.post(this.base_url+'applyForJob',formData1);
  }

   public apply(formData,fileToUpload: File){


   const formData1 = new FormData();
   formData1.append('fileKey', fileToUpload, fileToUpload.name);
   formData1.append('name', formData.name);
   formData1.append('email', formData.email);
   formData1.append('mobile', formData.mobile);
   formData1.append('id', formData.id);
   

    return this.httpClient.post(this.base_url+'apply',formData1);
  }

  public getSavedJobs(userid){
   return this.httpClient.get(this.base_url+'savedJobs/'+userid);
  }

  public getMatchedJobs(userid){
   return this.httpClient.get(this.base_url+'matchedJobs/'+userid);
  }

  public deleteSavedJob(id,userid){
   return this.httpClient.get(this.base_url+'deleteSavedJob/'+id+'/'+userid);
  }

  public getAppliedJobs(userid,usertype){
   return this.httpClient.get(this.base_url+'appliedJobs/'+userid+'/'+usertype);
  }

  public resumeAction(action,userid,jobseekers){

     const formData1 = new FormData();
     formData1.append('action', action);
     formData1.append('userid', userid);
     formData1.append('jobseekers', jobseekers);

    return this.httpClient.post(this.base_url+'resumeAction',formData1);
  }

  public downloadResumes(resumes){

     const formData1 = new FormData();
     formData1.append('resumes', resumes);

    return this.httpClient.post(this.base_url+'downloadResumes',formData1);
  }









  

}
