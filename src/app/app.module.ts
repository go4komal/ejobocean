import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { MultiSelectModule } from '@progress/kendo-angular-dropdowns';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/header/header.component';
import { FooterComponent } from './pages/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegisterEmployerComponent } from './pages/register-employer/register-employer.component';
import { LoginComponent } from './pages/login/login.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { BlogComponent } from './pages/blog/blog.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { JobsComponent } from './pages/jobs/jobs.component';
import { GovernmentJobsComponent } from './pages/government-jobs/government-jobs.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { TermsConditionComponent } from './pages/terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { FaqsComponent } from './pages/faqs/faqs.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { JobDetailComponent } from './pages/job-detail/job-detail.component';
import { PrivacySettingsComponent } from './pages/privacy-settings/privacy-settings.component';
import { BlockCompaniesComponent } from './pages/block-companies/block-companies.component';
import { VerifyEmailComponent } from './pages/verify-email/verify-email.component';
import { EmailVerificationComponent } from './pages/email-verification/email-verification.component';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FileuploadComponent } from './pages/fileupload/fileupload.component';
import { EmployerProfileComponent } from './pages/employer-profile/employer-profile.component';
import { ContactPersonsComponent } from './pages/contact-persons/contact-persons.component';
import { EmployersDashboardComponent } from './pages/employers-dashboard/employers-dashboard.component';
import { WorkExperienceComponent } from './pages/work-experience/work-experience.component';
import { EducationComponent } from './pages/education/education.component';
import { PreferrencesComponent } from './pages/preferrences/preferrences.component';
import { OtherDetailsComponent } from './pages/other-details/other-details.component';
import { LanguagesComponent } from './pages/languages/languages.component';
import { JobSetionComponent } from './pages/job-setion/job-setion.component';
import { PostJobComponent } from './pages/post-job/post-job.component';
import { SavedEmployersJobsComponent } from './pages/saved-employers-jobs/saved-employers-jobs.component';
import { PostedJobsComponent } from './pages/posted-jobs/posted-jobs.component';
import { EditJobComponent } from './pages/edit-job/edit-job.component';
import { ViewJobComponent } from './pages/view-job/view-job.component';
import { JobsbylocationComponent } from './pages/jobsbylocation/jobsbylocation.component';
import { JobsbyprofileComponent } from './pages/jobsbyprofile/jobsbyprofile.component';
import { Select2Module } from 'ng2-select2';
import { SavedJobsComponent } from './pages/saved-jobs/saved-jobs.component';
import { AppliedJobsComponent } from './pages/applied-jobs/applied-jobs.component';
import { JobApplicationsComponent } from './pages/job-applications/job-applications.component';
import { JobsbyemployerComponent } from './pages/jobsbyemployer/jobsbyemployer.component';
import { JobsbydateComponent } from './pages/jobsbydate/jobsbydate.component';
import { MatchedJobsComponent } from './pages/matched-jobs/matched-jobs.component';

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import {NgxPaginationModule} from 'ngx-pagination';
import { ManageLettersComponent } from './pages/manage-letters/manage-letters.component';
import { SendEmailComponent } from './pages/send-email/send-email.component';
import { InboxComponent } from './pages/inbox/inbox.component';
import { OutboxComponent } from './pages/outbox/outbox.component';
import { EmailComponent } from './pages/email/email.component';
import { ResumeSearchComponent } from './pages/resume-search/resume-search.component';
import { SavedResumesComponent } from './pages/saved-resumes/saved-resumes.component';
import { JobseekerProfileComponent } from './pages/jobseeker-profile/jobseeker-profile.component';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { LoginWithLinkedinComponent } from './pages/login-with-linkedin/login-with-linkedin.component';
import { LoaderComponent } from './pages/loader/loader.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { LoaderService } from './loader.service';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { LoaderInterceptor } from './loader.interceptor';
import { InterviewerComponent } from './pages/interviewer/interviewer.component';

import { SlickCarouselModule } from 'ngx-slick-carousel';

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("115343868175-satr3hq76o0frtm5u5nli0t5mcd8bsnl.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("496636424534579")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RegisterComponent,
    RegisterEmployerComponent,
    LoginComponent,
    AboutUsComponent,
    BlogComponent,
    BlogsComponent,
    JobsComponent,
    GovernmentJobsComponent,
    ContactUsComponent,
    TermsConditionComponent,
    PrivacyPolicyComponent,
    FaqsComponent,
    DashboardComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    JobDetailComponent,
    FileuploadComponent,
    EmployerProfileComponent,
    ContactPersonsComponent,
    EmployersDashboardComponent,
    WorkExperienceComponent,
    EducationComponent,
    PreferrencesComponent,
    OtherDetailsComponent,
    LanguagesComponent,
    JobSetionComponent,
    PostJobComponent,
    SavedEmployersJobsComponent,
    PostedJobsComponent,
    EditJobComponent,
    ViewJobComponent,
    JobsbylocationComponent,
    JobsbyprofileComponent,
    SavedJobsComponent,
    AppliedJobsComponent,
    JobApplicationsComponent,
    JobsbyemployerComponent,
    JobsbydateComponent,
    MatchedJobsComponent,
    PrivacySettingsComponent,
    BlockCompaniesComponent,
    VerifyEmailComponent,
    EmailVerificationComponent,
    ManageLettersComponent,
    SendEmailComponent,
    InboxComponent,
    OutboxComponent,
    EmailComponent,
    ResumeSearchComponent,
    SavedResumesComponent,
    JobseekerProfileComponent,
    LoginWithLinkedinComponent,
    LoaderComponent,
    InterviewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    Select2Module,
    AngularEditorModule,
    DatePickerModule,
    BrowserAnimationsModule,
    MultiSelectModule,
    NgxPaginationModule,
    SocialLoginModule,
    NgbModule,
    SlickCarouselModule
  ],
  providers: [
  LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
