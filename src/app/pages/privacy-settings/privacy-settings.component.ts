import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-privacy-settings',
  templateUrl: './privacy-settings.component.html',
  styleUrls: ['./privacy-settings.component.css']
})
export class PrivacySettingsComponent implements OnInit {
 
  submitted = false;
  error;
  settings;
  userid = localStorage.getItem('id');
  registerForm = this.fb.group({
	  userid : [this.userid],
	  settings : ['']

  });

  constructor(private fb: FormBuilder,private apiService: ApiService) { }

  ngOnInit() {

     this.apiService.getSettings(this.userid).subscribe((data:any)=>{
      //console.log(data);
       this.settings = data;
       
    });
    
  }


  onSubmit() {
   
     this.error  = ''; 
   //document.getElementById("thanks").style.display = "none";

   this.submitted = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.registerForm.value);



   var checkboxes = document.getElementsByName("checkboxes");
   var checkboxesChecked = [];

   for (var i=0; i<checkboxes.length; i++) {
      var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
      if ((<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked) {
        checkboxesChecked.push(inputValue);
     }
   }

   /* if(checkboxesChecked.length == 0)
   { 
     this.error  = 'Please select atleast one item';
     return; 
   }
   */

   this.registerForm.value.settings = checkboxesChecked;


	this.apiService.updateSetting(this.registerForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        //$('#contactForm').hide();
        //this.registerForm.reset();
        this.error  = 'Saved Sucessfully';
        //document.getElementById("thanks").style.display = "block"; 
      }
      if(data.status == false)
      {
        this.submitted = true;
        //$('#contactForm').hide();
          this.error  = data.error; 
        //document.getElementById("thanks").style.display = "none"; 
      }
      
    });
	}



}
