import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.css']
})
export class SendEmailComponent implements OnInit {


   p: number = 1;
  lettertypes;
  jobseekers;
  submitted;
  error;
  letters;
  userid = localStorage.getItem('id');
  email = localStorage.getItem('email');
  emailForm = this.fb.group({
      userid : [this.userid],
      from: [this.email],
      letter: [''],
      jobseekers: [''],
      body: ['', [Validators.required]],
      subject: ['', [Validators.required]],
  });

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }
  
  ngOnInit() {


    this.apiService.getEmployerLetters(this.userid).subscribe((data)=>{
      //console.log(data);
       this.letters = data;
    });

     this.apiService.getJobseekers().subscribe((data)=>{
      //console.log(data);
       this.jobseekers = data;
    });
  }

  selectLetter(event: Event)
  {

	  if(this.emailForm.value.letter.length > 0)
	  {
	    this.apiService.editEmployerLetter(this.emailForm.value.letter).subscribe((data:any)=>{
	      
	        this.emailForm.controls.from.setValue(data.FromEmail);
	        this.emailForm.controls.body.setValue(data.Body);
	        this.emailForm.controls.subject.setValue(data.Title);

	    });

	  }
	  else
	  {
	        this.emailForm.controls.from.setValue(this.email);
	        this.emailForm.controls.body.setValue('');
	        this.emailForm.controls.subject.setValue('');
	  }

   

  }

    get f() { return this.emailForm.controls; }

     onSubmit() {
   
    this.error  = ''; 
  // document.getElementById("thanks").style.display = "none";



   this.submitted = true;
  // TODO: Use EventEmitter with form value
  //console.warn(this.emailForm.value);

  if ( this.emailForm.value.body.length <= 0 && this.emailForm.value.subject.length <= 0) {
            return;
    }

     var checkboxes = document.getElementsByName("checkboxes");
   var checkboxesChecked = [];

   for (var i=0; i<checkboxes.length; i++) {
      var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
      if ((<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked) {
        checkboxesChecked.push(inputValue);
     }
   }

    if(checkboxesChecked.length == 0)
   { 
     this.error  = 'Please select atleast one jobseeker';
     return; 
   }
   

   this.emailForm.value.jobseekers = checkboxesChecked;


  this.apiService.sendEmail(this.emailForm.value).subscribe((data:any)=>{
       
       this.router.navigate(['/outbox']);
      
    });
  }

}
