import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-government-jobs',
  templateUrl: './government-jobs.component.html',
  styleUrls: ['./government-jobs.component.css']
})
export class GovernmentJobsComponent implements OnInit {

  p: number = 1;
  governmentjobs;
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  	this.apiService.getGovernmentJobs().subscribe((data:any)=>{
      console.log(data);
      this.governmentjobs = data;
    });
  }

}
