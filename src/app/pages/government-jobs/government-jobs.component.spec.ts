import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernmentJobsComponent } from './government-jobs.component';

describe('GovernmentJobsComponent', () => {
  let component: GovernmentJobsComponent;
  let fixture: ComponentFixture<GovernmentJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernmentJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernmentJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
