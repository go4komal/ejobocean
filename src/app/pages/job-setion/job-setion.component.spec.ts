import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobSetionComponent } from './job-setion.component';

describe('JobSetionComponent', () => {
  let component: JobSetionComponent;
  let fixture: ComponentFixture<JobSetionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobSetionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobSetionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
