import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-jobseeker-profile',
  templateUrl: './jobseeker-profile.component.html',
  styleUrls: ['./jobseeker-profile.component.css']
})
export class JobseekerProfileComponent implements OnInit {
  
  userid;
   experiences;
  alleducation;
  preferrences;
   languages;
  user;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

  this.userid = this.route.snapshot.paramMap.get("id");

  this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
      this.user = data;
    });

      this.apiService.getExperiences(this.userid).subscribe((data)=>{
      //console.log(data);
       this.experiences = data;
    });

     this.apiService.getAllEducation(this.userid).subscribe((data)=>{
      //console.log(data);
       this.alleducation = data;
    });

      this.apiService.getPreferrences(this.userid).subscribe((data)=>{
      //console.log(data);
       this.preferrences = data;
    });

      this.apiService.getLanguages(this.userid).subscribe((data)=>{
      //console.log(data);
       this.languages = data;
    });

   
  }

}
