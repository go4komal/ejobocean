import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jobsbyemployer',
  templateUrl: './jobsbyemployer.component.html',
  styleUrls: ['./jobsbyemployer.component.css']
})
export class JobsbyemployerComponent implements OnInit {
  p: number = 1;
  id;
  jobs;
  employer;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
  this.id = this.route.snapshot.paramMap.get("id");

  this.apiService.getJobsByEmployer(this.id).subscribe((data:any)=>{
      console.log(data);
      this.jobs = data.jobs;
      this.employer = data.user;
    });
  }

}
