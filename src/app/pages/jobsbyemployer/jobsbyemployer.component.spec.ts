import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsbyemployerComponent } from './jobsbyemployer.component';

describe('JobsbyemployerComponent', () => {
  let component: JobsbyemployerComponent;
  let fixture: ComponentFixture<JobsbyemployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsbyemployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsbyemployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
