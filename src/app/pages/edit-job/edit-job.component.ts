import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';
import { AngularEditorModule } from '@kolkov/angular-editor';

@Component({
  selector: 'app-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.css']
})
export class EditJobComponent implements OnInit {
	
  user;
  jobs;
  cities;
  countries;
  states;
  industries;
  profiles;
  designations;
  currencies;
  contacts;
  ugqualifications;
  pgqualifications;
  doctoratequalifications;
  submitted = false;
  error;
  jobtype = '';
  contactpersons;
  years = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
  ctc =  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18.19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
  lakhs =  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18.19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
  thousands =  [0,10,20,30,40,50,60,70,80,90];
  contractperiod = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];
  joiningmonth = [];

  userid = localStorage.getItem('id');
   jobForm = this.fb.group({
    id: [ this.route.snapshot.paramMap.get("id")],
    userid: [this.userid],
	  jobtype: ['', [Validators.required]],
	  copyfrom: ['0'],
	  title: ['', [Validators.required]],
	  keywords: ['', [Validators.required]],
	  vacancies: ['', [Validators.required,Validators.pattern("^[0-9]*$")]],
	  candidature: ['Local', [Validators.required]],
	  description: ['', [Validators.required]],
	  minexp: ['Fresher', [Validators.required]],
	  maxexp: ['Fresher', [Validators.required]],
	  currency: ['', [Validators.required]],
	  minctc: ['0', [Validators.required]],
	  maxctc: ['0', [Validators.required]],
	  ctcstatus: ['Public', [Validators.required]],
	  countries: [[], [Validators.required]],
    states: [[], [Validators.required]],
    cities: [[], [Validators.required]], 
    industries: [[], [Validators.required]],
    profiles: [[], [Validators.required]],
    designations: [[], [Validators.required]],
	  expdate: ['', [Validators.required]],
	  condition: [''],
	   ugqualifications: [[]],
    pgqualifications: [[]],
    doctoratequalifications: [[]],
	     contacts: [[], [Validators.required]],
	  workfromhome: ['Yes'],
	  billingcycle: ['Monthly'],
	  lakhs: ['0'],
	  thousands: ['0'],
	  joiningmonth: [''],
	  contractperiod: ['Less Than One'],


	});

	submitted1 = false;
  error1;
	contactpersonForm = this.fb.group({
	  firstname: ['', [Validators.required]],
	  lastname: ['', [Validators.required]],
	  mobile: ['', [Validators.required,Validators.pattern("^[0-9]{10}$")]],
	  email: ['', [Validators.required,Validators.email]],
	  contact: [''],
	  companyid : [this.userid],
	  id : [0],
      designation: ['', [Validators.required]],
	  designationid: ['', [Validators.required]],
	});



  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

  	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

    var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	

	 this.jobForm.controls.joiningmonth.setValue(months[month+1]+' '+year);

	//console.log(year+' '+month);

    for(var i = month+1  ; i<12 ; i++)
    {
    	this.joiningmonth.push(months[i]+' '+year);
    	if(i == 11)
    	year = year+1
    }

     for(var i = 0  ; i<month+1 ; i++)
    {
    	this.joiningmonth.push(months[i]+' '+year);
    	
    }

     this.apiService.getEmployerJobs(this.userid,'').subscribe((data)=>{
       this.jobs = data;
    });

  	this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
  		this.user = data;
   
       // console.log(this.jobForm.controls.industries.value);
       
  	});

    this.apiService.getIndustries().subscribe((data1)=>{
       this.industries = data1;
    });

  	this.apiService.getIndustries().subscribe((data)=>{
       this.industries = data;
    });

    this.apiService.getCountries().subscribe((data)=>{
       this.countries = data;
    });



   

    this.apiService.getProfiles().subscribe((data)=>{
       this.profiles = data;
    });

    this.apiService.getDesignations().subscribe((data)=>{
       this.designations = data;
    });

     this.apiService.getSpecializationsByQualification(5).subscribe((data1)=>{
      //console.log(data);
       this.ugqualifications = data1;
    });

     this.apiService.getSpecializationsByQualification(6).subscribe((data1)=>{
      //console.log(data);
       this.pgqualifications = data1;
    });

     this.apiService.getSpecializationsByQualification(7).subscribe((data1)=>{
      //console.log(data);
       this.doctoratequalifications = data1;
    });

    this.apiService.getCurrencies().subscribe((data)=>{
      //console.log(data);
       this.currencies = data;
    });


     this.apiService.getContactPersons(this.userid).subscribe((data)=>{
      //console.log(data);
       this.contacts = data;
    })

	this.apiService.getJobDetail(this.jobForm.value.id).subscribe((data:any)=>{
      //console.log(data);
      // this.specializations = data1;
      this.jobtype = data.JobType;
      this.jobForm.controls.jobtype.setValue(data.JobType);
      this.jobForm.controls.title.setValue(data.Title);
      this.jobForm.controls.keywords.setValue(data.Keywords);
      this.jobForm.controls.vacancies.setValue(data.Vacancies);
      this.jobForm.controls.description.setValue(data.Description);
      this.jobForm.controls.candidature.setValue(data.Candidature);
      this.jobForm.controls.minexp.setValue(data.MinExperience);
      this.jobForm.controls.maxexp.setValue(data.MaxExperience);
      this.jobForm.controls.currency.setValue(data.Currency);
      this.jobForm.controls.minctc.setValue(data.MinCTC);
      this.jobForm.controls.maxctc.setValue(data.MaxCTC);
      this.jobForm.controls.ctcstatus.setValue(data.CtcStatus);
      this.jobForm.controls.expdate.setValue( new Date(data.strtotime * 1000));
      this.jobForm.controls.condition.setValue(data.JobCondition);
      this.jobForm.controls.workfromhome.setValue(data.WorkFromHome);
      this.jobForm.controls.billingcycle.setValue(data.BillingCycle);
      this.jobForm.controls.lakhs.setValue(data.Lakhs);
      this.jobForm.controls.thousands.setValue(data.Thousands);
      this.jobForm.controls.joiningmonth.setValue(data.JoiningMonth);
      this.jobForm.controls.contractperiod.setValue(data.ContractPeriod);

    });

     


    this.apiService.getJobContacts(this.jobForm.value.id).subscribe((data:any)=>{
      //console.log(data);
      this.contactpersons = data.contacts;

      if(data.ids.length > 0)
      {
        this.jobForm.controls.contacts.setValue(data.ids);
      }
      

    });


    this.apiService.getJobLocations(this.jobForm.value.id).subscribe((data:any)=>{
      //console.log(data);
      this.states = data.states;
      this.cities = data.cities;

       this.jobForm.controls.countries.setValue(data.countryids);
      this.jobForm.controls.states.setValue(data.stateids);
      this.jobForm.controls.cities.setValue(data.cityids);

    });

      this.apiService.getJobRoles(this.jobForm.value.id).subscribe((data:any)=>{
      //console.log(data);
      

      this.jobForm.controls.industries.setValue(data.industryids);
      this.jobForm.controls.profiles.setValue(data.profileids);
      this.jobForm.controls.designations.setValue(data.designationids);


    });


      this.apiService.getJobQualifications(this.jobForm.value.id).subscribe((data:any)=>{
      //console.log(data);
      

      if(data.UGids.length > 0)
      {
        this.jobForm.controls.ugqualifications.setValue(data.UGids);

      }

      if(data.PGids.length > 0)
      {

        this.jobForm.controls.pgqualifications.setValue(data.PGids);

      }

       if(data.Doctorateids.length > 0)
      {
        this.jobForm.controls.doctoratequalifications.setValue(data.Doctorateids);

      }

    });

  }

  typeChanged(event: Event)
  {
   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.jobtype=selectElementText;
  }

   countryChanged()
  {
  	 this.apiService.getStatesByCountries(this.jobForm.value.countries).subscribe((data)=>{
          //console.log(data);
           this.states = data;
     })


  }


  

       stateChanged()
  {
    

    this.apiService.getCitiesByStates(this.jobForm.value.states).subscribe((data)=>{
          //console.log(data);
           this.cities = data;
        })


  }

  contactChanged()
  {
    

    this.apiService.getContactsByIds(this.jobForm.value.contacts).subscribe((data)=>{
      //console.log(data);
       this.contactpersons = data;
  })


  }

  get f() { return this.jobForm.controls; }

  onSubmit() {
   
   this.error  = ''; 

   this.submitted = true;
  // TODO: Use EventEmitter with form value
  

  if (this.jobForm.invalid) {
  console.warn(this.jobForm.value);

            return;
    }

     this.apiService.postJob(this.jobForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {

        this.router.navigate(['/saved-employers-jobs']);

      }
      if(data.status == false)
      {
        this.submitted = true;
        this.error  = data.error; 
      }
      
    });

 
  }

  designationChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.contactpersonForm.controls.designation.setValue(selectElementText);

  }


 
  get f1() { return this.contactpersonForm.controls; }

  
  onSubmit1() {
   
   this.error1  = ''; 
   

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.contactpersonForm.value);

	if (this.contactpersonForm.invalid) {
            return;
    }


	this.apiService.saveContactPerson(this.contactpersonForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted1 = false;
        this.contacts = data.contacts;
        this.contactpersonForm.controls.firstname.setValue('');
        this.contactpersonForm.controls.lastname.setValue('');
        this.contactpersonForm.controls.email.setValue('');
        this.contactpersonForm.controls.mobile.setValue('');
        this.contactpersonForm.controls.contact.setValue('');
        this.contactpersonForm.controls.designation.setValue('');
        this.contactpersonForm.controls.designationid.setValue('');
        this.contactpersonForm.controls.id.setValue(0);
        //document.getElementById("contactform").style.display = "none";
         document.getElementById("close").click();
        
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
       
      }
      
    });
	}


}
