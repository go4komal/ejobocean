import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-preferrences',
  templateUrl: './preferrences.component.html',
  styleUrls: ['./preferrences.component.css']
})
export class PreferrencesComponent implements OnInit {

  
  preferrences;
  countries;
  states;
  cities;
  submitted1 = false;
  error1;
  userid = localStorage.getItem('id');

   
  preferrenceForm = this.fb.group({
	  countryid: ['', [Validators.required]],
	  stateid: ['', [Validators.required]],
	  country: ['', [Validators.required]],
	  state: ['', [Validators.required]],
	  cityid: ['', [Validators.required]],
	  city: ['', [Validators.required]],
	  userid : [this.userid],
	  id : [0],
	});

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { 


  }

  ngOnInit() {

    this.apiService.getCountries().subscribe((data)=>{
      //console.log(data);
       this.countries = data;
    });
     
     this.apiService.getPreferrences(this.userid).subscribe((data)=>{
      //console.log(data);
       this.preferrences = data;
    });

   

  }


  countryChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.preferrenceForm.controls.country.setValue(selectElementText);

    this.apiService.getStatesByCountry(this.preferrenceForm.value.countryid).subscribe((data)=>{
      //console.log(data);
       this.states = data;
    });

  }

  stateChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.preferrenceForm.controls.state.setValue(selectElementText);

    this.apiService.getCitiesByState(this.preferrenceForm.value.stateid).subscribe((data)=>{
      //console.log(data);
       this.cities = data;
    });

  }

  cityChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.preferrenceForm.controls.city.setValue(selectElementText);

  }


  getUserType()
  {
    return localStorage.getItem('usertype');
  }


 
  get f1() { return this.preferrenceForm.controls; }

  onSubmit1() {
   
   this.error1  = ''; 
   

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	

	if (this.preferrenceForm.invalid) {
	console.warn(this.preferrenceForm.value);
            return;
    }


	this.apiService.savePreferrence(this.preferrenceForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted1 = false;
        this.preferrences = data.preferences;
        this.preferrenceForm.controls.countryid.setValue('');
        this.preferrenceForm.controls.stateid.setValue('');
        this.preferrenceForm.controls.country.setValue('');
        this.preferrenceForm.controls.state.setValue('');
        this.preferrenceForm.controls.cityid.setValue('');
        this.preferrenceForm.controls.city.setValue('');
        this.preferrenceForm.controls.id.setValue(0);
        this.states = [];
        this.cities = [];
        
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
       
      }
      
    });
	}

   deletePreferrence(id) {
   
  
	this.apiService.deletePreferrence(id,this.userid).subscribe((data:any)=>{
      
        this.preferrences = data;
      
    });
}

 editPreferrence(id) {
   
  
	this.apiService.editPreferrence(id).subscribe((data:any)=>{

        this.states = data.states;
        this.cities = data.cities;
        this.preferrenceForm.controls.countryid.setValue(data.CountryId);
        this.preferrenceForm.controls.stateid.setValue(data.StateId);
        this.preferrenceForm.controls.country.setValue(data.Country);
        this.preferrenceForm.controls.state.setValue(data.State);
        this.preferrenceForm.controls.cityid.setValue(data.CityId);
        this.preferrenceForm.controls.city.setValue(data.City);
        this.preferrenceForm.controls.id.setValue(data.Id);
    });
}


}
