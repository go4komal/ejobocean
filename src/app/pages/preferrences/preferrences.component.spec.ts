import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreferrencesComponent } from './preferrences.component';

describe('PreferrencesComponent', () => {
  let component: PreferrencesComponent;
  let fixture: ComponentFixture<PreferrencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreferrencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreferrencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
