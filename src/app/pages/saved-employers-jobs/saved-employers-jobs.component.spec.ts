import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedEmployersJobsComponent } from './saved-employers-jobs.component';

describe('SavedEmployersJobsComponent', () => {
  let component: SavedEmployersJobsComponent;
  let fixture: ComponentFixture<SavedEmployersJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedEmployersJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedEmployersJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
