import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-saved-employers-jobs',
  templateUrl: './saved-employers-jobs.component.html',
  styleUrls: ['./saved-employers-jobs.component.css']
})
export class SavedEmployersJobsComponent implements OnInit {
   
    p: number = 1;
  jobs;
  countries;
  industries;
  userid = localStorage.getItem('id');
  filterForm = this.fb.group({
    userid : [this.userid],
    approved : ['No'],
    countries: [''],
    industries: [''],
    skill: [''],
    experience:[''],
    title:['']
  });

  constructor(private fb: FormBuilder,private apiService: ApiService) { }

  ngOnInit() {

   this.apiService.getEmployerJobs(this.userid,'No').subscribe((data)=>{
      //console.log(data);
       this.jobs = data;
    });

    this.apiService.getCountries().subscribe((data)=>{
       this.countries = data;
    });

    this.apiService.getIndustries().subscribe((data)=>{
       this.industries = data;
    });


  }

  onSubmit()
  {
     this.apiService.filterEmployerJobs(this.filterForm.value).subscribe((data:any)=>{
      this.jobs = data;
    });

  }


   Reset()
  {
     
     this.filterForm.controls.countries.setValue('');
     this.filterForm.controls.industries.setValue('');
     this.filterForm.controls.skill.setValue('');
     this.filterForm.controls.experience.setValue('');
     this.filterForm.controls.title.setValue('');

     this.onSubmit();
  }

   deleteJob(id) {
   
  
  this.apiService.deleteJob(id,this.userid,'No').subscribe((data:any)=>{
      
        this.jobs = data;
      
      });
  }

   editJob(id) {
     
    
    
  }

}
