import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsbylocationComponent } from './jobsbylocation.component';

describe('JobsbylocationComponent', () => {
  let component: JobsbylocationComponent;
  let fixture: ComponentFixture<JobsbylocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsbylocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsbylocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
