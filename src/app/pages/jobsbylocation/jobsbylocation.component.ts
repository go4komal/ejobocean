import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jobsbylocation',
  templateUrl: './jobsbylocation.component.html',
  styleUrls: ['./jobsbylocation.component.css']
})
export class JobsbylocationComponent implements OnInit {
   p: number = 1;
  id;
  jobs;
  location;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
  this.id = this.route.snapshot.paramMap.get("id");

  this.apiService.getJobsByLocation(this.id).subscribe((data:any)=>{
      console.log(data);
      this.jobs = data.jobs;
      this.location = data.city;
    });
  }

}
