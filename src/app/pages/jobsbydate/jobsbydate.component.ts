import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jobsbydate',
  templateUrl: './jobsbydate.component.html',
  styleUrls: ['./jobsbydate.component.css']
})
export class JobsbydateComponent implements OnInit {
  p: number = 1;
  id;
  jobs;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
  this.id = this.route.snapshot.paramMap.get("id");

  this.apiService.getJobsByDate(this.id).subscribe((data:any)=>{
      console.log(data);
      this.jobs = data.jobs;
    });
  }

}
