import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jobsbyprofile',
  templateUrl: './jobsbyprofile.component.html',
  styleUrls: ['./jobsbyprofile.component.css']
})
export class JobsbyprofileComponent implements OnInit {
 p: number = 1;
  id;
  jobs;
  profile;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
  this.id = this.route.snapshot.paramMap.get("id");

  this.apiService.getJobsByProfile(this.id).subscribe((data:any)=>{
      console.log(data);
      this.jobs = data.jobs;
      this.profile = data.profile;
    });
  }

}
