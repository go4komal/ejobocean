import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsbyprofileComponent } from './jobsbyprofile.component';

describe('JobsbyprofileComponent', () => {
  let component: JobsbyprofileComponent;
  let fixture: ComponentFixture<JobsbyprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsbyprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsbyprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
