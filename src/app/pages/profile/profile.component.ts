import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  
  cities;
  countries;
  states;
  industries;
  profiles;
  designations;
  contacts;
  currencies;
  logo;
  resume;
  submitted = false;
  error;
  submitted1 = false;
  error1;
  userid = localStorage.getItem('id');
  fileToUpload: File = null;
  fileToUpload1: File = null;
  years = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18.19,20];
  months = [0,1,2,3,4,5,6,7,8,9,10,11];
  user;
  profileForm = this.fb.group({
    name: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
    mobile: ['', [Validators.required,Validators.pattern("^[0-9]{10}$")]],
    email: ['', [Validators.required,Validators.email]],
    address: ['', [Validators.required]],
    userid : [''],
      city: ['', [Validators.required]],
      cityid: ['', [Validators.required]],
      state: ['', [Validators.required]],
      stateid: ['', [Validators.required]],
      country: ['', [Validators.required]],
      countryid: ['', [Validators.required]],
      industry: ['', [Validators.required]],
      industryid: ['', [Validators.required]],
      profile: ['', [Validators.required]],
      profileid: ['', [Validators.required]],
      designation: ['', [Validators.required]],
      designationid: ['', [Validators.required]],
      expyears: ['', [Validators.required]],
      expmonths: ['', [Validators.required]],
      logo: [''],
      ctc: ['', [Validators.pattern("^[0-9.]*$")]],
      currency: [''],
      currentlocation: ['', [Validators.required]],
      outsideindia: ['No'],
      skill: [''],
      dob: ['',[Validators.required]],
      gender: ['',[Validators.required]],
      landline: ['', [Validators.pattern("^[0-9-]*$")]],
      maritalstatus: [''],
      caddress: ['',[Validators.required]],
      pincode: ['', [Validators.pattern("^[0-9]{6}$")]],
      linkedin: [''],
      resume : [''],
  });

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {

 this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{

        this.user = data;

       this.profileForm.setValue({name: data.Name,mobile: data.Mobile,city:data.City,cityid:data.CityId,address: data.Address,userid:data.Id,email:data.Email,state:data.State,stateid:data.StateId,country:data.Country,countryid:data.CountryId,industry:data.Industry,industryid:data.IndustryId,logo:data.Logo,expyears:data.ExpYears,expmonths:data.ExpMonths,ctc:data.CurrentSalary,currency:data.SalaryCurrency,currentlocation:data.CurrentLocation,outsideindia:data.OutsideIndia,profile:data.Profile,profileid:data.ProfileId,designation:data.Designation,designationid:data.DesignationId,skill:data.Skills,dob:new Date(data.strtotime * 1000),gender:data.Gender,caddress:data.CorrespondenceAddress,landline:data.LandlineNumber,pincode:data.PinCode,linkedin:data.LinkedInProfile,maritalstatus:data.MaritalStatus,resume:data.Resume});

         this.logo = data.image;

          this.resume = data.ResumePath;


        this.apiService.getIndustries().subscribe((data1)=>{
      //console.log(data);
       this.industries = data1;
    });

    this.apiService.getCountries().subscribe((data2)=>{
      //console.log(data);
       this.countries = data2;
    });


    
       if(data.IndustryId == 0)
       this.profileForm.controls.industryid.setValue('');

       if(data.CountryId == 0)
       this.profileForm.controls.countryid.setValue('');
       else
       {
          this.apiService.getStatesByCountry(this.profileForm.value.countryid).subscribe((data3:any)=>{
           //console.log(data);
           this.states = data3;
           });
       }

       if(data.StateId == 0)
       this.profileForm.controls.stateid.setValue('');
       else
       {

       this.apiService.getCitiesByState(this.profileForm.value.stateid).subscribe((data4)=>{
          //console.log(data);
           this.cities = data4;
        });

       }

       if(data.CityId == 0)
       this.profileForm.controls.cityid.setValue('');

        if(data.DesignationId == 0)
       this.profileForm.controls.designationid.setValue('');

       if(data.IndustryId == 0)
       this.profileForm.controls.industryid.setValue('');
     

       
    });

     
   this.apiService.getDesignations().subscribe((data)=>{
      //console.log(data);
       this.designations = data;
    });

     this.apiService.getCurrencies().subscribe((data)=>{
      //console.log(data);
       this.currencies = data;
    });

      this.apiService.getProfiles().subscribe((data)=>{
       this.profiles = data;
    });
   

  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }

  handleFileInput(files: FileList) {

    this.fileToUpload = files.item(0);
  }

  handleFileInput1(files: FileList) {

    this.fileToUpload1 = files.item(0);
  }

  industryChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.industry.setValue(selectElementText);

  }

  profileChanged(event: Event)
  {
   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.profile.setValue(selectElementText);

  }

  countryChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.country.setValue(selectElementText);

    this.apiService.getStatesByCountry(this.profileForm.value.countryid).subscribe((data)=>{
      //console.log(data);
       this.states = data;
    });

  }

  stateChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.state.setValue(selectElementText);

    this.apiService.getCitiesByState(this.profileForm.value.stateid).subscribe((data)=>{
      //console.log(data);
       this.cities = data;
    });

  }

  cityChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.city.setValue(selectElementText);

  }


  designationChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.designation.setValue(selectElementText);

  }

  get f() { return this.profileForm.controls; }

  onSubmit() {
   
    this.error  = ''; 
   document.getElementById("thanks").style.display = "none";



   this.submitted = true;
  // TODO: Use EventEmitter with form value
  //console.warn(this.profileForm.value);

  if(this.fileToUpload !== null)
  {
    var filename = this.fileToUpload.name;
    var res = filename.split(".");
    if(res.length !== 2)
    {
     
       this.error  = 'Please upload image file with extension jpg,jpeg or png'; 
       return;

    }
    else
    {
      var ext = res[1].toLowerCase();
      if(ext !== 'jpg' && ext !== 'jpeg' && ext !== 'png')
      {
    
        this.error  = 'Please upload image file with extension jpg,jpeg or png'; 
          return;
      }
    }

  }

  if (this.profileForm.invalid) {
            return;
    }

    if(this.fileToUpload1 !== null)
  {
    var filename = this.fileToUpload1.name;
    var res = filename.split(".");
    if(res.length !== 2)
    {
     
       this.error  = 'Please upload resume file with extension doc,pdf or docx'; 
       return;

    }
    else
    {
      var ext = res[1].toLowerCase();
      if(ext !== 'doc' && ext !== 'docx' && ext !== 'pdf')
      {
    
      this.error  = 'Please upload resume file with extension doc,pdf or docx';
          return;
      }
    }

  }


  this.apiService.updateProfile(this.profileForm.value,this.fileToUpload,this.fileToUpload1).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        this.logo = data.image;
        this.resume = data.resumepath;
        this.profileForm.controls.logo.setValue(data.imagename);
        this.profileForm.controls.resume.setValue(data.resume);
        localStorage.setItem('name', this.profileForm.value.name);
        (<HTMLInputElement>document.getElementById("file")).value = ""; 
        (<HTMLInputElement>document.getElementById("file1")).value = ""; 
        document.getElementById("thanks").style.display = "block"; 
        this.router.navigate(['/work-experience']);

      }
      if(data.status == false)
      {
        this.submitted = true;
        this.error  = data.error; 
        document.getElementById("thanks").style.display = "none"; 
      }
      
    });
  }

}
