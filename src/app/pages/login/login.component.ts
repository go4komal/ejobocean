import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider,SocialUser } from "angularx-social-login";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  private user: SocialUser;
  private loggedIn: boolean;
  submitted = false;
  error;
  loginForm = this.fb.group({
	  email: ['', [Validators.required,Validators.email]],
	  password: ['', [Validators.required]],
	  usertype:['jobseeker']
	});

  submitted1 = false;
  error1;
  loginForm1 = this.fb.group({
	  email: ['', [Validators.required,Validators.email]],
	  password: ['', [Validators.required]],
	  usertype:['employer']
	});

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router,private authService: AuthService) { }

  ngOnInit() {
  }

  get f() { return this.loginForm.controls; }

  get f1() { return this.loginForm1.controls; }

  onSubmit() {
   
     this.error  = ''; 
   document.getElementById("thanks").style.display = "none";

   this.submitted = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.loginForm.value);

	 if (this.loginForm.invalid) {
            return;
        }

  
	this.apiService.login(this.loginForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        this.loginForm.reset();
        //document.getElementById("thanks").style.display = "block"; 
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('id', data.user.Id);
        localStorage.setItem('name', data.user.Name);
        localStorage.setItem('email', data.user.Email);
        localStorage.setItem('mobile', data.user.Mobile);
        localStorage.setItem('usertype', data.user.UserType);
        this.router.navigate(['/dashboard']);
        
      }
      if(data.status == false)
      {
        this.submitted = true;
        this.error  = data.error; 
        document.getElementById("thanks").style.display = "none"; 
      }
      
    });
	}

	onSubmit1() {
   
     this.error1  = ''; 
   document.getElementById("thanks1").style.display = "none";

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.loginForm1.value);

	 if (this.loginForm1.invalid) {
            return;
        }

  
	this.apiService.login(this.loginForm1.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        this.loginForm.reset();
        //document.getElementById("thanks").style.display = "block"; 
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('id', data.user.Id);
        localStorage.setItem('name', data.user.Name);
        localStorage.setItem('email', data.user.Email);
        localStorage.setItem('mobile', data.user.Mobile);
        localStorage.setItem('usertype', data.user.UserType);
        this.router.navigate(['/employers-dashboard']);
         
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
        document.getElementById("thanks1").style.display = "none"; 
      }
      
    });
	}

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);

     this.authService.authState.subscribe((user) => {

      this.user = user;
      this.loggedIn = (user != null);

      if(this.loggedIn == true)
      {
          this.apiService.loginWithGoogle(this.user).subscribe((data:any)=>{
          if(data.status == true)
          {
            this.submitted = false;
            this.loginForm.reset();
            //document.getElementById("thanks").style.display = "block"; 
            localStorage.setItem('isLoggedIn', "true");
            localStorage.setItem('id', data.user.Id);
            localStorage.setItem('name', data.user.Name);
            localStorage.setItem('email', data.user.Email);
            localStorage.setItem('mobile', data.user.Mobile);
            localStorage.setItem('usertype', data.user.UserType);
            this.router.navigate(['/dashboard']);
            
          }
          if(data.status == false)
          {
            this.submitted = true;
            this.error  = data.error; 
            document.getElementById("thanks").style.display = "none"; 
          }
          
        });
      }
    });
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);

    this.authService.authState.subscribe((user) => {

      this.user = user;
       //console.log(this.user);
      this.loggedIn = (user != null);

      if(this.loggedIn == true)
      {
          this.apiService.loginWithFacebook(this.user).subscribe((data:any)=>{
          if(data.status == true)
          {
            this.submitted = false;
            this.loginForm.reset();
            //document.getElementById("thanks").style.display = "block"; 
            localStorage.setItem('isLoggedIn', "true");
            localStorage.setItem('id', data.user.Id);
            localStorage.setItem('name', data.user.Name);
            localStorage.setItem('email', data.user.Email);
            localStorage.setItem('mobile', data.user.Mobile);
            localStorage.setItem('usertype', data.user.UserType);
            this.router.navigate(['/dashboard']);
            
          }
          if(data.status == false)
          {
            this.submitted = true;
            this.error  = data.error; 
            document.getElementById("thanks").style.display = "none"; 
          }
          
        });
      }
    });
  } 


}
