import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-outbox',
  templateUrl: './outbox.component.html',
  styleUrls: ['./outbox.component.css']
})
export class OutboxComponent implements OnInit {
 
  p: number = 1;
  mails;
   userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

    this.apiService.outbox(this.userid).subscribe((data)=>{
      //console.log(data);
       this.mails = data;
    });
  }

   getUserType()
  {
    return localStorage.getItem('usertype');
  }

}
