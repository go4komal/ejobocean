import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginWithLinkedinComponent } from './login-with-linkedin.component';

describe('LoginWithLinkedinComponent', () => {
  let component: LoginWithLinkedinComponent;
  let fixture: ComponentFixture<LoginWithLinkedinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginWithLinkedinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginWithLinkedinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
