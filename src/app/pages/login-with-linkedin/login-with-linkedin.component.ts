import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-login-with-linkedin',
  templateUrl: './login-with-linkedin.component.html',
  styleUrls: ['./login-with-linkedin.component.css']
})
export class LoginWithLinkedinComponent implements OnInit {
 

  
  constructor(private router: Router,private route: ActivatedRoute,private apiService: ApiService) { }

  ngOnInit() {

  	 var url = window.location.href;
  	 var urlarray = url.split('?');
  	 if(urlarray.length == 2)
  	 {
  	 	var params = urlarray[1];
  	 	var paramsarray = params.split('&');
  	 	var code = paramsarray[0];
  	 	var codearray = code.split('=');
  	 	if(codearray[0] == 'code')
  	 	{
  	 		this.apiService.loginWithLinkedIn(codearray[1]).subscribe((data:any)=>{

  	 				if(data.status == true)
			          {
			            //document.getElementById("thanks").style.display = "block"; 
			            localStorage.setItem('isLoggedIn', "true");
			            localStorage.setItem('id', data.user.Id);
			            localStorage.setItem('name', data.user.Name);
			            localStorage.setItem('email', data.user.Email);
			            localStorage.setItem('mobile', data.user.Mobile);
			            localStorage.setItem('usertype', data.user.UserType);
			            this.router.navigate(['/dashboard']);
			            
			          }
			          else
			          {
			            this.router.navigate(['/login']); 
			          }
          
		      
		    });

  	 	}
  	 	else
	  	 {
	  	   this.router.navigate(['/login']);
	  	 }

  	 }
  	 else
  	 {
  	   this.router.navigate(['/login']);
  	 }
  }

}
