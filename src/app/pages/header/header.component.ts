import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Meta } from '@angular/platform-browser';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider,SocialUser } from "angularx-social-login";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    
    private user: SocialUser;
  private loggedIn: boolean;
  constructor(private meta: Meta,private router: Router,private authService: AuthService) { 
   /*this.meta.updateTag({ name: 'keywords', content: 'bhangarwale,waste management,recycle waste,paper waste,plastic waste,e-waste,save environment' });*/
  }

  ngOnInit() {
  
  if(localStorage.getItem("isLoggedIn") === null)
     localStorage.setItem('isLoggedIn','false');
  }

  logout()
  {

    localStorage.clear();
    localStorage.setItem('isLoggedIn','false');
     this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);

      if(this.loggedIn == true)
      {
        this.authService.signOut();
      }
    });
   
     this.router.navigate(['/']);
  }

  isLoggedIn()
  {
    return localStorage.getItem('isLoggedIn');
  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }

}
