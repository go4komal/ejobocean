import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  id;
  blog;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
	  this.id = this.route.snapshot.paramMap.get("id");

	  this.apiService.getBlog(this.id).subscribe((data:any)=>{
	      this.blog = data;
	  });
  }

}
