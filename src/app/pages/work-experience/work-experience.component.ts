import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.css']
})
export class WorkExperienceComponent implements OnInit {

  
  experiences;
  submitted1 = false;
  error1;
  userid = localStorage.getItem('id');
   months = [0,1,2,3,4,5,6,7,8,9,10,11,12];
   
  experienceForm = this.fb.group({
	  companyname: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  employertype: ['', [Validators.required]],
	  fromyear: ['', [Validators.required]],
	  toyear: ['', [Validators.required]],
	  designation: ['',[Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  userid : [this.userid],
	  id : [0],
      profile: ['',[Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  noticeperiod: [''],
	});

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {
     
     this.apiService.getExperiences(this.userid).subscribe((data)=>{
      //console.log(data);
       this.experiences = data;
    });

   

  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }

  typeChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   if(selectElementText == 'Current')
   {
   		this.experienceForm.controls.toyear.setValue('Ongoing');
   		(<HTMLInputElement>document.getElementById("toyear")).disabled  = true; 	
   }
   else
   {

   		this.experienceForm.controls.toyear.setValue('');
   		(<HTMLInputElement>document.getElementById("toyear")).disabled  = false; 
   }


  }

 
  get f1() { return this.experienceForm.controls; }

  
  onSubmit1() {
   
   this.error1  = ''; 
   

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	

	if (this.experienceForm.invalid) {
	console.warn(this.experienceForm.value);
            return;
    }


	this.apiService.saveExperience(this.experienceForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted1 = false;
        this.experiences = data.experiences;
        this.experienceForm.controls.companyname.setValue('');
        this.experienceForm.controls.employertype.setValue('');
        this.experienceForm.controls.fromyear.setValue('');
        this.experienceForm.controls.toyear.setValue('');
        this.experienceForm.controls.profile.setValue('');
        this.experienceForm.controls.designation.setValue('');
        this.experienceForm.controls.noticeperiod.setValue('');
        this.experienceForm.controls.id.setValue(0);
        
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
       
      }
      
    });
	}

   deleteExperience(id) {
   
  
	this.apiService.deleteExperience(id,this.userid).subscribe((data:any)=>{
      
        this.experiences = data;
      
    });
}

 editExperience(id) {
   
  
	this.apiService.editExperience(id).subscribe((data:any)=>{
      
         this.experienceForm.controls.companyname.setValue(data.CompanyName);
        this.experienceForm.controls.employertype.setValue(data.EmployerType);
        this.experienceForm.controls.fromyear.setValue(new Date(data.strtotime1 * 1000));
        this.experienceForm.controls.toyear.setValue(new Date(data.strtotime2 * 1000));
        this.experienceForm.controls.profile.setValue(data.Profile);
        this.experienceForm.controls.designation.setValue(data.Designation);
        this.experienceForm.controls.noticeperiod.setValue(data.NoticePeriod);
        this.experienceForm.controls.id.setValue(data.Id);
    });
}


}
