import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 

  experiences;
  alleducation;
  preferrences;
   languages;
  user;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService,private router: Router) { }

  ngOnInit() {

  this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
      this.user = data;
    });

      this.apiService.getExperiences(this.userid).subscribe((data)=>{
      //console.log(data);
       this.experiences = data;
    });

     this.apiService.getAllEducation(this.userid).subscribe((data)=>{
      //console.log(data);
       this.alleducation = data;
    });

      this.apiService.getPreferrences(this.userid).subscribe((data)=>{
      //console.log(data);
       this.preferrences = data;
    });

      this.apiService.getLanguages(this.userid).subscribe((data)=>{
      //console.log(data);
       this.languages = data;
    });

   


  }

}
