import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-other-details',
  templateUrl: './other-details.component.html',
  styleUrls: ['./other-details.component.css']
})
export class OtherDetailsComponent implements OnInit {
  
  categories;
  submitted;
  error;
  userid = localStorage.getItem('id');
  profileForm = this.fb.group({
    userid : [''],
      category: ['', [Validators.required]],
      categoryid: ['', [Validators.required]],
      expectedsalary: ['', [Validators.required,Validators.pattern("^[0-9.]*$")]],
      physicallychallenged: ['No'],
      passport: ['No'],
      license: ['No'],
      description: [''],
  });

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {

    this.apiService.getCastCategories().subscribe((data)=>{
      //console.log(data);
       this.categories = data;
    });

 this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{

       this.profileForm.setValue({category:data.CastCategory,categoryid:data.CastCategoryId,description: data.Description,userid:data.Id,passport:data.Passport,license:data.DrivingLicense,expectedsalary:data.ExpectedSalary,physicallychallenged:data.PhysicallyChallenged});

       
    });

     
 
   

  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }


  categoryChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.category.setValue(selectElementText);

  }

  

  get f() { return this.profileForm.controls; }

  onSubmit() {
   
    this.error  = ''; 
   document.getElementById("thanks").style.display = "none";



   this.submitted = true;
  // TODO: Use EventEmitter with form value
  //console.warn(this.profileForm.value);

  if (this.profileForm.invalid) {
            return;
    }

  this.apiService.updateOther(this.profileForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        document.getElementById("thanks").style.display = "block"; 
        this.router.navigate(['/dashboard']);

      }
      if(data.status == false)
      {
        this.submitted = true;
        this.error  = data.error; 
        document.getElementById("thanks").style.display = "none"; 
      }
      
    });
  }

}
