import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-register-employer',
  templateUrl: './register-employer.component.html',
  styleUrls: ['./register-employer.component.css']
})
export class RegisterEmployerComponent implements OnInit {

  submitted = false;
  error;
  registerForm = this.fb.group({
	  name: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  mobile: ['', [Validators.required,Validators.pattern("^[0-9]{10}$")]],
	  email: ['', [Validators.required,Validators.email]],
	  password: ['', [Validators.required,Validators.minLength(6)]],
	  cpassword: ['', [Validators.required]],
	  usertype : ['employer'],
	  settings : ['']

	});

  constructor(private fb: FormBuilder,private apiService: ApiService) { }

  ngOnInit() {
    
  }



  get f() { return this.registerForm.controls; }

  onSubmit() {
   
     this.error  = ''; 
   document.getElementById("thanks").style.display = "none";

   this.submitted = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.registerForm.value);

	 if (this.registerForm.invalid) {
            return;
        }




    if(this.registerForm.value.password !== this.registerForm.value.cpassword)
    {
      //console.log('in first if');
       this.error = 'Password and confirm password should be same';
       return; 
    }

       var checkboxes = document.getElementsByName("accept");
   var checkboxesChecked = [];

   for (var i=0; i<checkboxes.length; i++) {
      var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
      if ((<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked) {
        checkboxesChecked.push(inputValue);
     }
   }

   if(checkboxesChecked.length == 0)
   { 
   //console.log('in second if');
     this.error  = 'Please accept terms and conditions';
     return; 
   }






	this.apiService.register(this.registerForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        //$('#contactForm').hide();
        this.registerForm.reset();
        document.getElementById("thanks").style.display = "block"; 
      }
      if(data.status == false)
      {
        this.submitted = true;
        //$('#contactForm').hide();
          this.error  = data.error; 
        document.getElementById("thanks").style.display = "none"; 
      }
      
    });
	}

}
