import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-posted-jobs',
  templateUrl: './posted-jobs.component.html',
  styleUrls: ['./posted-jobs.component.css']
})
export class PostedJobsComponent implements OnInit {
  
   p: number = 1;
  jobs;
  userid = localStorage.getItem('id');
  countries;
  industries;
  filterForm = this.fb.group({
    userid : [this.userid],
    approved : ['Yes'],
    countries: [''],
    industries: [''],
    skill: [''],
    experience:[''],
    title:['']
  });

  constructor(private fb: FormBuilder,private apiService: ApiService) { }

  ngOnInit() {

   this.apiService.getEmployerJobs(this.userid,'Yes').subscribe((data)=>{
      //console.log(data);
       this.jobs = data;
    });

    this.apiService.getCountries().subscribe((data)=>{
       this.countries = data;
    });

    this.apiService.getIndustries().subscribe((data)=>{
       this.industries = data;
    });


  }

  onSubmit()
  {
     this.apiService.filterEmployerJobs(this.filterForm.value).subscribe((data:any)=>{
      this.jobs = data;
    });

  }


   Reset()
  {
     
     this.filterForm.controls.countries.setValue('');
     this.filterForm.controls.industries.setValue('');
     this.filterForm.controls.skill.setValue('');
     this.filterForm.controls.experience.setValue('');
     this.filterForm.controls.title.setValue('');

     this.onSubmit();
  }

    deleteJob(id) {
   
  
	this.apiService.deleteJob(id,this.userid,'Yes').subscribe((data:any)=>{
      
        this.jobs = data;
      
	    });
	}

	 editJob(id) {
	   
	  
		
	}


}
