import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-saved-resumes',
  templateUrl: './saved-resumes.component.html',
  styleUrls: ['./saved-resumes.component.css']
})
export class SavedResumesComponent implements OnInit {
   
  p: number = 1;
   p1: number = 1;
  savedresumes;
  deletedresumes;
  error;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

     this.apiService.getSavedResumes(this.userid,'0').subscribe((data:any)=>{
        //console.log(data);
        this.savedresumes = data;
      });

      this.apiService.getSavedResumes(this.userid,'1').subscribe((data:any)=>{
        //console.log(data);
        this.deletedresumes = data;
      });
  }


  selectall()
  {

  var checkboxes = document.getElementsByName("checkboxes");
     if ((<HTMLInputElement>document.getElementById('selectall')).checked) {
          
          for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = true;
           }
       }
     else
     {

     for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = false;
           }

     }
  }

  selectall1()
  {

  var checkboxes = document.getElementsByName("checkboxes1");
     if ((<HTMLInputElement>document.getElementById('selectall1')).checked) {
          
          for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = true;
           }
       }
     else
     {

     for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = false;
           }

     }
  }

  deleteResume(id) {
   
  
    this.apiService.deleteSavedResume(this.userid,id).subscribe((data:any)=>{
      
         this.savedresumes = data.saved;
         this.deletedresumes = data.deleted;
      
    });

  }

  download()
  {

    this.error  = ''; 

     var checkboxes = document.getElementsByName("checkboxes");
     var checkboxes1 = document.getElementsByName("checkboxes1");
     var checkboxesChecked = [];

     for (var i=0; i<checkboxes.length; i++) {
        var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
        if ((<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked) {
          checkboxesChecked.push(inputValue);
       }
     }

     for (var i=0; i<checkboxes1.length; i++) {
        var inputValue = (<HTMLInputElement>document.getElementById(checkboxes1[i].id)).value;
        if ((<HTMLInputElement>document.getElementById(checkboxes1[i].id)).checked) {
          checkboxesChecked.push(inputValue);
       }
     }

     if(checkboxesChecked.length == 0)
     { 
       this.error  = 'Please select atleast one resume';
       return; 
     }

      for (var i=0; i<checkboxesChecked.length; i++) { 
        
        
        // document.getElementById('download_'+checkboxesChecked[i]).click(); 
        //console.log(checkboxesChecked[i]);
	    window.open(checkboxesChecked[i]);
	    //window.location.href = checkboxesChecked[i];
       
     }


     (<HTMLInputElement>document.getElementById('selectall')).checked = false;
     (<HTMLInputElement>document.getElementById('selectall1')).checked = false;


     var checkboxes = document.getElementsByName("checkboxes");

           for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = false;
           }

           var checkboxes1 = document.getElementsByName("checkboxes1");

           for (var i=0; i<checkboxes1.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes1[i].id)).checked = false;
           }

  }

}
