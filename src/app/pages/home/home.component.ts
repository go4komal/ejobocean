import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
   p: number = 1;
    p1: number = 1;
    p2:number = 1;
  jobs;
  governmentjobs;
  profiles;
  countries;
  jobprofiles;
  submitted = false;
  error;
  interviewers;
  years = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
  filterForm = this.fb.group({
    countries: [''],
    profiles: [''],
    skill: [''],
    experience:[''],
    ctc:['']
  });

   slides :any = [];
 
  slideConfig = {
    "slidesToShow": 3, 
    "slidesToScroll": 1,
    "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",
    "dots":true,
    "infinite": false
  };
  
  
  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {

  	this.apiService.getFeaturedJobs().subscribe((data:any)=>{
      //console.log(data);
      this.jobs = data;
    });

    this.apiService.getFeaturedGovernmentJobs().subscribe((data:any)=>{
      //console.log(data);
      this.governmentjobs = data;
    });

    this.apiService.getJobProfiles().subscribe((data:any)=>{
      //console.log(data);
      this.jobprofiles = data;
    });

    this.apiService.getCountries().subscribe((data)=>{
       this.countries = data;
    });

    this.apiService.getProfiles().subscribe((data)=>{
       this.profiles = data;
    });

    this.apiService.getInterviewers().subscribe((data)=>{
       this.interviewers = data;
        this.slides = data;
    });


   
  }

   onSubmit()
    {
      sessionStorage.setItem('filter','Yes');
      sessionStorage.setItem('countries',this.filterForm.value.countries);
      sessionStorage.setItem('profiles',this.filterForm.value.profiles);
      sessionStorage.setItem('skill',this.filterForm.value.skill);
      sessionStorage.setItem('experience',this.filterForm.value.experience);
      sessionStorage.setItem('ctc',this.filterForm.value.ctc);
      this.router.navigate(['/jobs']);
    }

   
  
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  
 

}
