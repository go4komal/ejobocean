import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  
  alleducation;
  qualifications;
  specializations;
  submitted1 = false;
  error1;
  userid = localStorage.getItem('id');
  years = [];
   
  educationForm = this.fb.group({
	  qualification: ['', [Validators.required]],
	  qualificationid: ['', [Validators.required]],
	  specialization: ['', [Validators.required]],
	  specializationid: ['', [Validators.required]],
	  university: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  institute: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  coursetype: ['', [Validators.required]],
	  yearofpassing: ['', [Validators.required]],
	  marks: ['', [Validators.required,Validators.pattern("^[0-9]{2}")]],
	  grade: [''],
	  userid : [this.userid],
	  id : [0],
	});

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { 

  	var d = new Date();
  	for(var i = d.getFullYear() - 50 ; i<= d.getFullYear() ; i++)
  	{
  		this.years.push(i);
  	}

  }

  ngOnInit() {

    this.apiService.getQualifications().subscribe((data)=>{
      //console.log(data);
       this.qualifications = data;
    });
     
     this.apiService.getAllEducation(this.userid).subscribe((data)=>{
      //console.log(data);
       this.alleducation = data;
    });

   

  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }

  

  qualificationChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.educationForm.controls.qualification.setValue(selectElementText);
   this.educationForm.controls.specializationid.setValue("");
   this.apiService.getSpecializationsByQualification(this.educationForm.value.qualificationid).subscribe((data1)=>{
	      //console.log(data);
	       this.specializations = data1;
	    });

  }

   specializationChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.educationForm.controls.specialization.setValue(selectElementText);

  }

 
  get f1() { return this.educationForm.controls; }

  onSubmit1() {
   
   this.error1  = ''; 
   

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	

	if (this.educationForm.invalid) {
	console.warn(this.educationForm.value);
            return;
    }


	this.apiService.saveEducation(this.educationForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted1 = false;
        this.alleducation = data.alleducation;
        this.educationForm.controls.qualification.setValue('');
        this.educationForm.controls.qualificationid.setValue('');
        this.educationForm.controls.specialization.setValue('');
        this.educationForm.controls.specializationid.setValue('');
        this.educationForm.controls.university.setValue('');
        this.educationForm.controls.institute.setValue('');
        this.educationForm.controls.coursetype.setValue('');
        this.educationForm.controls.yearofpassing.setValue('');
        this.educationForm.controls.marks.setValue('');
        this.educationForm.controls.grade.setValue('');
        this.educationForm.controls.id.setValue(0);
        
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
       
      }
      
    });
	}

   deleteEducation(id) {
   
  
	this.apiService.deleteEducation(id,this.userid).subscribe((data:any)=>{
      
        this.alleducation = data;
      
    });
}

 editEducation(id) {
   
  
	this.apiService.editEducation(id).subscribe((data:any)=>{

		this.apiService.getSpecializationsByQualification( data.QualificationId).subscribe((data1:any)=>{
	      console.log(data1);
	       this.specializations = data1;
	    });
      
        this.educationForm.controls.qualification.setValue(data.Qualification);
        this.educationForm.controls.qualificationid.setValue(data.QualificationId);
        this.educationForm.controls.specialization.setValue(data.Specialization);
        this.educationForm.controls.specializationid.setValue(data.SpecializationId);
        this.educationForm.controls.university.setValue(data.University);
        this.educationForm.controls.institute.setValue(data.Institute);
        this.educationForm.controls.coursetype.setValue(data.CourseType);
        this.educationForm.controls.yearofpassing.setValue(data.YearOfPassing);
        this.educationForm.controls.marks.setValue(data.Marks);
        this.educationForm.controls.grade.setValue(data.Grade);
        this.educationForm.controls.id.setValue(data.Id);
    });
}


}
