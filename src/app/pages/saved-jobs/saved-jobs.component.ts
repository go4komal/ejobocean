import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-saved-jobs',
  templateUrl: './saved-jobs.component.html',
  styleUrls: ['./saved-jobs.component.css']
})
export class SavedJobsComponent implements OnInit {
   
    p: number = 1;
  jobs;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  	this.apiService.getSavedJobs(this.userid).subscribe((data)=>{
       //console.log(data);
       this.jobs = data;
    });

  }

  deleteJob(id) {
  this.apiService.deleteSavedJob(id,this.userid).subscribe((data:any)=>{
      
        this.jobs = data;
      
      });
  }

}
