import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employer-profile',
  templateUrl: './employer-profile.component.html',
  styleUrls: ['./employer-profile.component.css']
})
export class EmployerProfileComponent implements OnInit {

  cities;
  countries;
  states;
  industries;
  contacts;
  logo;
  submitted = false;
  error;
  userid = localStorage.getItem('id');
   fileToUpload: File = null;
  profileForm = this.fb.group({
	  name: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  mobile: ['', [Validators.required,Validators.pattern("^[0-9]{10}$")]],
	  email: ['', [Validators.required,Validators.email]],
	  address: ['', [Validators.required]],
	  userid : [''],
      city: ['', [Validators.required]],
	    cityid: ['', [Validators.required]],
	    state: ['', [Validators.required]],
	    stateid: ['', [Validators.required]],
	    country: ['', [Validators.required]],
	    countryid: ['', [Validators.required]],
	    industry: ['', [Validators.required]],
	    industryid: ['', [Validators.required]],
	    companyname: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	    companytype: ['', [Validators.required]],
	    companywebsite: [''],
	     contact: [''],
	     fax: [''],
	      logo: [''],
	     empcount: ['', [Validators.pattern("^[0-9]*$")]],
	     tinno: ['', [Validators.pattern("^[0-9-]{10}$")]],
	     panno: ['',[Validators.required,Validators.pattern("^[a-zA-Z0-9]{10}$")]],
	});



  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {

 
  	this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{

       this.profileForm.setValue({name: data.Name,mobile: data.Mobile,city:data.City,cityid:data.CityId,address: data.Address,userid:data.Id,email:data.Email,state:data.State,stateid:data.StateId,country:data.Country,countryid:data.CountryId,industry:data.Industry,industryid:data.IndustryId,companyname:data.CompanyName,companywebsite:data.CompanyWebsite,contact:data.ContactNo,fax:data.FaxNo,companytype:data.CompanyType,empcount:data.EmployeeCount,tinno:data.TinNo,panno:data.PanNo,logo:data.Logo});

         this.logo = data.image;

        this.apiService.getIndustries().subscribe((data1)=>{
      //console.log(data);
       this.industries = data1;
    });

    this.apiService.getCountries().subscribe((data2)=>{
      //console.log(data);
       this.countries = data2;
    });


       if(data.EmployeeCount == 0)
       this.profileForm.controls.empcount.setValue('');

       if(data.IndustryId == 0)
       this.profileForm.controls.industryid.setValue('');

       if(data.CountryId == 0)
       this.profileForm.controls.countryid.setValue('');
       else
       {
       		this.apiService.getStatesByCountry(this.profileForm.value.countryid).subscribe((data3:any)=>{
           //console.log(data);
           this.states = data3;
           });
       }

       if(data.StateId == 0)
       this.profileForm.controls.stateid.setValue('');
       else
       {

       this.apiService.getCitiesByState(this.profileForm.value.stateid).subscribe((data4)=>{
          //console.log(data);
           this.cities = data4;
        });

       }

       if(data.CityId == 0)
       this.profileForm.controls.cityid.setValue('');

       if(data.CompanyType == null)
       this.profileForm.controls.companytype.setValue('');

        if(data.CompanyWebsite == null)
       this.profileForm.controls.companywebsite.setValue('');

       if(data.ContactNo == null)
       this.profileForm.controls.contact.setValue('');

       if(data.TinNo == null)
       this.profileForm.controls.tinno.setValue('');


     

       

       
    });

     


   

  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }

  handleFileInput(files: FileList) {

    this.fileToUpload = files.item(0);
  }

  industryChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.industry.setValue(selectElementText);

  }

  countryChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.country.setValue(selectElementText);

    this.apiService.getStatesByCountry(this.profileForm.value.countryid).subscribe((data)=>{
      //console.log(data);
       this.states = data;
    });

  }

  stateChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.state.setValue(selectElementText);

    this.apiService.getCitiesByState(this.profileForm.value.stateid).subscribe((data)=>{
      //console.log(data);
       this.cities = data;
    });

  }

  cityChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.profileForm.controls.city.setValue(selectElementText);

  }


 
  get f() { return this.profileForm.controls; }
  

  onSubmit() {
   
   this.error  = ''; 
   document.getElementById("thanks").style.display = "none";



   this.submitted = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.profileForm.value);

	if(this.fileToUpload !== null)
	{
		var filename = this.fileToUpload.name;
		var res = filename.split(".");
		if(res.length !== 2)
		{
		 
		   this.error  = 'Please upload image file with extension jpg,jpeg or png'; 
		   return;

		}
		else
		{
			var ext = res[1].toLowerCase();
			if(ext !== 'jpg' && ext !== 'jpeg' && ext !== 'png')
			{
		
				this.error  = 'Please upload image file with extension jpg,jpeg or png'; 
		   		return;
			}
		}

	}

	if (this.profileForm.invalid) {
            return;
    }


	this.apiService.updateEmployerProfile(this.profileForm.value,this.fileToUpload).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        this.logo = data.image;
        this.profileForm.controls.logo.setValue(data.imagename);
        localStorage.setItem('name', this.profileForm.value.name);
        document.getElementById("thanks").style.display = "block"; 
        this.router.navigate(['/contact-persons']);

      }
      if(data.status == false)
      {
        this.submitted = true;
        this.error  = data.error; 
        document.getElementById("thanks").style.display = "none"; 
      }
      
    });
	}

  

}
