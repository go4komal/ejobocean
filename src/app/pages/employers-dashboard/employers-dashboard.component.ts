import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employers-dashboard',
  templateUrl: './employers-dashboard.component.html',
  styleUrls: ['./employers-dashboard.component.css']
})
export class EmployersDashboardComponent implements OnInit {
 

  contacts;
  user;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService,private router: Router) { }

  ngOnInit() {

  this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
      this.user = data;
    });

       this.apiService.getContactPersons(this.userid).subscribe((data)=>{
      //console.log(data);
       this.contacts = data;
    });



  }

}
