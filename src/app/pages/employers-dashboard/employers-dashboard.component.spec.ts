import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployersDashboardComponent } from './employers-dashboard.component';

describe('EmployersDashboardComponent', () => {
  let component: EmployersDashboardComponent;
  let fixture: ComponentFixture<EmployersDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployersDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployersDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
