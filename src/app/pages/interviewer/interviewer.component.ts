import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-interviewer',
  templateUrl: './interviewer.component.html',
  styleUrls: ['./interviewer.component.css']
})
export class InterviewerComponent implements OnInit {
  
  id;
  interviewer;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

   this.id = this.route.snapshot.paramMap.get("id");

   this.apiService.getInterviewer(this.id).subscribe((data:any)=>{
     // console.log(data);
      this.interviewer = data;

    });
  }

}
