import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageLettersComponent } from './manage-letters.component';

describe('ManageLettersComponent', () => {
  let component: ManageLettersComponent;
  let fixture: ComponentFixture<ManageLettersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageLettersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
