import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-manage-letters',
  templateUrl: './manage-letters.component.html',
  styleUrls: ['./manage-letters.component.css']
})
export class ManageLettersComponent implements OnInit {
  
  lettertypes;
  contacts;
  submitted;
  error;
  letters;
  user;
  userid = localStorage.getItem('id');
  letterForm = this.fb.group({
      id : [0],
      userid : [this.userid],
      lettertype: ['', [Validators.required]],
      from: ['', [Validators.required]],
      purpose: ['', [Validators.required]],
      body: ['', [Validators.required]],
      subject: ['', [Validators.required]],
  });
  constructor(private fb: FormBuilder,private apiService: ApiService) { }

  ngOnInit() {

  this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
      this.user = data;
    })

    this.apiService.getContactPersons(this.userid).subscribe((data)=>{
      //console.log(data);
       this.contacts = data;
    });

  	this.apiService.getEmployerLetterTypes().subscribe((data)=>{
      //console.log(data);
       this.lettertypes = data;
    });

    this.apiService.getEmployerLetters(this.userid).subscribe((data)=>{
      //console.log(data);
       this.letters = data;
    });
  }

   get f() { return this.letterForm.controls; }

   onSubmit() {
   
    this.error  = ''; 
  // document.getElementById("thanks").style.display = "none";



   this.submitted = true;
  // TODO: Use EventEmitter with form value
  //console.warn(this.letterForm.value);

  if (this.letterForm.invalid) {
            return;
    }

  this.apiService.saveEmployerLetter(this.letterForm.value).subscribe((data:any)=>{
       if(data.status == true)
      {
        this.submitted = false;
        this.letters = data.letters;
        this.letterForm.controls.lettertype.setValue('');
        this.letterForm.controls.from.setValue('');
        this.letterForm.controls.purpose.setValue('');
        this.letterForm.controls.body.setValue('');
         this.letterForm.controls.subject.setValue('');
      }
      if(data.status == false)
      {
        this.submitted = true;
        this.error  = data.error; 
       
      }
      
    });
  }

   deleteEmployerLetter(id) {
		this.apiService.deleteEmployerLetter(id,this.userid).subscribe((data:any)=>{ 
	        this.letters = data;
	    });
	}



   editEmployerLetter(id) {

	this.apiService.editEmployerLetter(id).subscribe((data:any)=>{
      
        this.letterForm.controls.lettertype.setValue(data.LetterTypeId);
        this.letterForm.controls.from.setValue(data.FromEmail);
        this.letterForm.controls.purpose.setValue(data.Purpose);
        this.letterForm.controls.body.setValue(data.Body);
        this.letterForm.controls.subject.setValue(data.Title);
        this.letterForm.controls.id.setValue(data.Id);
    });
}

}
