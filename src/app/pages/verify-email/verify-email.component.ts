import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

  user;
  userid = localStorage.getItem('id');
  message;
  constructor(private apiService: ApiService) { }

  ngOnInit() {

    this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
      this.user = data;
    });
  }
  
  verifyEmail()
  {
     this.apiService.sendVerifyEmail(this.userid).subscribe((data:any)=>{
      this.message = 'Verification mail sent to your registered email id';
    });
  }

   getUserType()
  {
    return localStorage.getItem('usertype');
  }

}
