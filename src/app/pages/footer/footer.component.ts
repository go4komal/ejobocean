import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  year = 2019;
  submitted = false;
  error = '';
  links;
  content;
  contactForm = this.fb.group({
    email: ['', [Validators.required,Validators.email]],
  });

  constructor(private fb: FormBuilder,private apiService: ApiService) {

  	var d = new Date();
	this.year = d.getFullYear();


   }

  ngOnInit() {


    this.apiService.getSocialLinks().subscribe((data)=>{
      //console.log(data);
       this.links = data;
    });

    this.apiService.getContent().subscribe((data)=>{
      //console.log(data);
       this.content = data;
    });
  }

  get f() { return this.contactForm.controls; }

  onSubmit() {

   this.submitted = true;
  // TODO: Use EventEmitter with form value
  //console.warn(this.contactForm.value);

   if (this.contactForm.invalid) {
            return;
        }


  this.apiService.postContact1(this.contactForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted = false;
        //$('#contactForm').hide();
        this.contactForm.reset();
        this.error = data.message;
      }
      else
      {
        this.error = data.message;
      }
      
    });
  }

}
