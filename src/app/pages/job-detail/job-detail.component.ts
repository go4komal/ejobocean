import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css']
})
export class JobDetailComponent implements OnInit {

  
  id;
  user;
  job;
  cities;
  countries;
  states;
  industries;
  profiles;
  designations;
  currencies;
  contacts;
  ugqualifications;
  pgqualifications;
  doctoratequalifications;
  qualifications;
  contactpersons;
  message;
  fileToUpload: File = null;
  
  userid = localStorage.getItem('id');


  submitted1 = false;
  error1;
  content;
  applyForm = this.fb.group({
    name: ['', [Validators.required]],
    mobile: ['', [Validators.required,Validators.pattern("^[0-9]{10}$")]],
    email: ['', [Validators.required,Validators.email]],
    id : [this.id],
  });


  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get("id");
    this.applyForm.controls.id.setValue(this.id); 

    if(this.userid !== null)
    {
      this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
         //console.log(data);
        this.user = data;
      });
    }
  	


     this.apiService.getContent().subscribe((data)=>{
      //console.log(data);
       this.content = data;
    });


	this.apiService.getJobDetail(this.id).subscribe((data:any)=>{
      //console.log(data);
      this.job = data;

    });

    this.apiService.getJobContacts(this.id).subscribe((data:any)=>{
     // console.log(data);
     this.contacts = data.contacts;
    });


    this.apiService.getJobLocations(this.id).subscribe((data:any)=>{
      //console.log(data);
      var q = '';
      this.countries = '';
      for(var i=0;i<data.jobcountries.length;i++)
      {
        this.countries += q+data.jobcountries[i].name;
        var q = ',';
      }
    });

    this.apiService.getJobRoles(this.id).subscribe((data:any)=>{
      //console.log(data);
       var q = '';
       this.industries = '';
      for(var i=0;i<data.jobindustries.length;i++)
      {
        this.industries += q+data.jobindustries[i].Title;
        var q = ',';
      }

       var q = '';
       this.profiles = '';
      for(var i=0;i<data.jobprofiles.length;i++)
      {
        this.profiles += q+data.jobprofiles[i].Title;
        var q = ',';
      }

       var q = '';
       this.designations = '';
      for(var i=0;i<data.jobdesignations.length;i++)
      {
        this.designations += q+data.jobdesignations[i].Title;
        var q = ',';
      }
    });


      this.apiService.getJobQualifications(this.id).subscribe((data:any)=>{
      //console.log(data);

      var q = '';
      this.qualifications = '';
      for(var i=0;i<data.jobugqualifications.length;i++)
      {
        this.qualifications += q+data.jobugqualifications[i].Title;
        var q = ',';
      }

      for(var i=0;i<data.jobpgqualifications.length;i++)
      {
        this.qualifications += q+data.jobpgqualifications[i].Title;
        var q = ',';
      }

      for(var i=0;i<data.jobdoctoratequalifications.length;i++)
      {
        this.qualifications += q+data.jobdoctoratequalifications[i].Title;
        var q = ',';
      }

    });

  }

   isLoggedIn()
  {
    return localStorage.getItem('isLoggedIn');
  }
  getUserType()
  {
    return localStorage.getItem('usertype');
  }



  saveJob() {

    if(this.userid == null){
        this.router.navigate(['/login']);
    }
    else{
        this.apiService.saveJob(this.id,this.userid).subscribe((data:any)=>{
           this.message = data.message;
          
        });
    }
  }

  applyForJob() {

    if(this.userid == null){
        this.router.navigate(['/login']);
    }
    else{
        this.apiService.applyForJob(this.id,this.userid).subscribe((data:any)=>{
           this.message = data.message;
          
        });
    }
  }

  handleFileInput(files: FileList) {

    this.fileToUpload = files.item(0);
  }

  get f1() { return this.applyForm.controls; }

  onSubmit1() {
   
   this.error1  = ''; 

   this.submitted1 = true;
  // TODO: Use EventEmitter with form value
  //console.warn(this.applyForm.value);


  if (this.applyForm.invalid) {
            return;
    }

    if(this.fileToUpload !== null)
  {
    var filename = this.fileToUpload.name;
    var res = filename.split(".");
    if(res.length !== 2)
    {
     
       this.error1  = 'Please upload resume file with extension doc,pdf or docx'; 
       return;

    }
    else
    {
      var ext = res[1].toLowerCase();
      if(ext !== 'doc' && ext !== 'docx' && ext !== 'pdf')
      {
    
      this.error1  = 'Please upload resume file with extension doc,pdf or docx';
          return;
      }
    }

  }
  else
  {
     this.error1  = 'Please upload resume file with extension doc,pdf or docx';
          return;
  }


  this.apiService.apply(this.applyForm.value,this.fileToUpload).subscribe((data:any)=>{

        this.submitted1 = false;
        this.message = data.message;
        this.applyForm.controls.name.setValue('');
        this.applyForm.controls.email.setValue('');
        this.applyForm.controls.mobile.setValue('');
        (<HTMLInputElement>document.getElementById("file")).value = ""; 
        document.getElementById("close").click(); 
      
    });

 
  }


  
}
