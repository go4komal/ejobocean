import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-contact-persons',
  templateUrl: './contact-persons.component.html',
  styleUrls: ['./contact-persons.component.css']
})
export class ContactPersonsComponent implements OnInit {


  designations;
  contacts;
  submitted1 = false;
  error1;
  userid = localStorage.getItem('id');
   
	contactpersonForm = this.fb.group({
	  firstname: ['', [Validators.required]],
	  lastname: ['', [Validators.required]],
	  mobile: ['', [Validators.required,Validators.pattern("^[0-9]{10}$")]],
	  email: ['', [Validators.required,Validators.email]],
	  contact: [''],
	  companyid : [this.userid],
	  id : [0],
      designation: ['', [Validators.required]],
	  designationid: ['', [Validators.required]],
	});

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {

 
     
 	 this.apiService.getDesignations().subscribe((data)=>{
      //console.log(data);
       this.designations = data;
    });

     this.apiService.getContactPersons(this.userid).subscribe((data)=>{
      //console.log(data);
       this.contacts = data;
    });

   

  }

  getUserType()
  {
    return localStorage.getItem('usertype');
  }

  

  designationChanged(event: Event)
  {

   let selectedOptions = event.target['options'];
   let selectedIndex = selectedOptions.selectedIndex;
   let selectElementText = selectedOptions[selectedIndex].text;
   this.contactpersonForm.controls.designation.setValue(selectElementText);

  }


 
  get f1() { return this.contactpersonForm.controls; }

  
  onSubmit1() {
   
   this.error1  = ''; 
   

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	//console.warn(this.contactpersonForm.value);

	if (this.contactpersonForm.invalid) {
            return;
    }


	this.apiService.saveContactPerson(this.contactpersonForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted1 = false;
        this.contacts = data.contacts;
        this.contactpersonForm.controls.firstname.setValue('');
        this.contactpersonForm.controls.lastname.setValue('');
        this.contactpersonForm.controls.email.setValue('');
        this.contactpersonForm.controls.mobile.setValue('');
        this.contactpersonForm.controls.contact.setValue('');
        this.contactpersonForm.controls.designation.setValue('');
        this.contactpersonForm.controls.designationid.setValue('');
        this.contactpersonForm.controls.id.setValue(0);
        
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
       
      }
      
    });
	}

   deleteContact(id) {
   
  
	this.apiService.deleteContactPerson(id,this.userid).subscribe((data:any)=>{
      
        this.contacts = data;
      
    });
}

 editContact(id) {
   
  
	this.apiService.editContactPerson(id).subscribe((data:any)=>{
      
        this.contactpersonForm.controls.firstname.setValue(data.FirstName);
        this.contactpersonForm.controls.lastname.setValue(data.LastName);
        this.contactpersonForm.controls.email.setValue(data.Email);
        this.contactpersonForm.controls.mobile.setValue(data.Mobile);
        this.contactpersonForm.controls.contact.setValue(data.Contact);
        this.contactpersonForm.controls.designation.setValue(data.Designation);
        this.contactpersonForm.controls.designationid.setValue(data.DesignationId);
        this.contactpersonForm.controls.id.setValue(data.Id);
    });
}


}
