import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
	
   p: number = 1;
  blogs;	
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  	this.apiService.getBlogs().subscribe((data:any)=>{
        //console.log(data);
        this.blogs = data;
      });
  }

}
