import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';


@Component({
  selector: 'app-job-applications',
  templateUrl: './job-applications.component.html',
  styleUrls: ['./job-applications.component.css']
})
export class JobApplicationsComponent implements OnInit {
  
   p: number = 1;
  jobs;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  	this.apiService.getAppliedJobs(this.userid,'employer').subscribe((data)=>{
       console.log(data);
       this.jobs = data;
    });

  }

}
