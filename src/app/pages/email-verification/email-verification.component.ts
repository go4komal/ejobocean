import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {
	
  code;
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

   this.code = this.route.snapshot.paramMap.get("code");
   console.log(this.code);
   this.apiService.verifyEmail(this.code).subscribe((data:any)=>{
       
       if(data.status == true)
      {
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('id', data.user.Id);
        localStorage.setItem('name', data.user.Name);
        localStorage.setItem('email', data.user.Email);
        localStorage.setItem('mobile', data.user.Mobile);
        localStorage.setItem('usertype', data.user.UserType);
        this.router.navigate(['/verify-email']);
        
      }
      else
      {
        this.router.navigate(['/']);
      }
      
    });
  }

}
