import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent implements OnInit {

  
  languages;
  submitted1 = false;
  error1;
  userid = localStorage.getItem('id');

   
  languageForm = this.fb.group({
	  language: ['', [Validators.required,Validators.pattern("^[a-zA-Z ]*$")]],
	  level: ['', [Validators.required]],
	  read: ['Yes'],
	  write: ['Yes'],
	  speak: ['Yes'],
	  userid : [this.userid],
	  id : [0],
	});

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { 


  }

  ngOnInit() {

     
     this.apiService.getLanguages(this.userid).subscribe((data)=>{
      //console.log(data);
       this.languages = data;
    });

   

  }




  getUserType()
  {
    return localStorage.getItem('usertype');
  }


 
  get f1() { return this.languageForm.controls; }

  onSubmit1() {
   
   this.error1  = ''; 
   

   this.submitted1 = true;
	// TODO: Use EventEmitter with form value
	

	if (this.languageForm.invalid) {
	console.warn(this.languageForm.value);
            return;
    }


	this.apiService.saveLanguage(this.languageForm.value).subscribe((data:any)=>{
      if(data.status == true)
      {
        this.submitted1 = false;
        this.languages = data.languages;
        this.languageForm.controls.language.setValue('');
        this.languageForm.controls.read.setValue('');
        this.languageForm.controls.write.setValue('');
        this.languageForm.controls.speak.setValue('');
        this.languageForm.controls.level.setValue('');
        this.languageForm.controls.id.setValue(0);
        
        
      }
      if(data.status == false)
      {
        this.submitted1 = true;
        this.error1  = data.error; 
       
      }
      
    });
	}

   deleteLanguage(id) {
   
  
	this.apiService.deleteLanguage(id,this.userid).subscribe((data:any)=>{
      
        this.languages = data;
      
    });
}

 editLanguage(id) {
   
  
	this.apiService.editLanguage(id).subscribe((data:any)=>{
        this.languageForm.controls.language.setValue(data.Language);
        this.languageForm.controls.read.setValue(data.IsRead);
        this.languageForm.controls.write.setValue(data.IsWrite);
        this.languageForm.controls.speak.setValue(data.IsSpeak);
        this.languageForm.controls.level.setValue(data.Level);
        this.languageForm.controls.id.setValue(data.Id);
    });
}


}
