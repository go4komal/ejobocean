import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-resume-search',
  templateUrl: './resume-search.component.html',
  styleUrls: ['./resume-search.component.css']
})
export class ResumeSearchComponent implements OnInit {
	
  p: number = 1;
  resumes;
  industries;
  profiles;
  designations;
  countries;
  error;
  message;
  userid = localStorage.getItem('id');
  exp = ['Fresher',1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,'More Than 20'];
  years = [18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60];
  filterForm = this.fb.group({
    name: [''],
    keyword: [''],
    experience: [''],
    agefrom: [''],
    ageto: [['']],
    countries: [[]],
    profiles: [[]],
    industries: [[]],
    designations: [[]],
    gender: ['']
  });

  constructor(private apiService: ApiService,private fb: FormBuilder) { }

  ngOnInit() {

  	  this.apiService.getResumes().subscribe((data:any)=>{
        //console.log(data);
        this.resumes = data;
      });

       this.apiService.getCountries().subscribe((data)=>{
       this.countries = data;
      });

      this.apiService.getProfiles().subscribe((data)=>{
         this.profiles = data;
      });

       this.apiService.getIndustries().subscribe((data)=>{
         this.industries = data;
      });

      this.apiService.getDesignations().subscribe((data)=>{
         this.designations = data;
      });
  }


  onSubmit()
  {
  
     this.apiService.filterResumes(this.filterForm.value).subscribe((data:any)=>{
      this.resumes = data;
    });

  }


   Reset()
  {
     
     
     this.filterForm.controls.countries.setValue('');
     this.filterForm.controls.profiles.setValue('');
     this.filterForm.controls.name.setValue('');
     this.filterForm.controls.keyword.setValue('');
     this.filterForm.controls.agefrom.setValue('');
     this.filterForm.controls.ageto.setValue('');
     this.filterForm.controls.experience.setValue('');
     this.filterForm.controls.gender.setValue('');
     this.filterForm.controls.industries.setValue('');
     this.filterForm.controls.designations.setValue('');
    this.onSubmit();
  }

  action(value)
  { 
    this.error  = ''; 
    this.message  = ''; 

     var checkboxes = document.getElementsByName("checkboxes");
     var checkboxesChecked = [];

     for (var i=0; i<checkboxes.length; i++) {
        var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
        if ((<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked) {
          checkboxesChecked.push(inputValue);
       }
     }

     if(checkboxesChecked.length == 0)
     { 
       this.error  = 'Please select atleast one resume';
       return; 
     }


        this.apiService.resumeAction(value,this.userid,checkboxesChecked).subscribe((data:any)=>{
          this.message = data.message;

           var checkboxes = document.getElementsByName("checkboxes");

           for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = false;
           }

           (<HTMLInputElement>document.getElementById('selectall')).checked = false;
        });
     
  }

  selectall()
  {

  var checkboxes = document.getElementsByName("checkboxes");
     if ((<HTMLInputElement>document.getElementById('selectall')).checked) {
          
          for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = true;
           }
       }
     else
     {

     for (var i=0; i<checkboxes.length; i++) {
              var inputValue = (<HTMLInputElement>document.getElementById(checkboxes[i].id)).value;
              (<HTMLInputElement>document.getElementById(checkboxes[i].id)).checked = false;
           }

     }
  }


}
