import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-view-job',
  templateUrl: './view-job.component.html',
  styleUrls: ['./view-job.component.css']
})
export class ViewJobComponent implements OnInit {
	
  id;
  user;
  job;
  cities;
  countries;
  states;
  industries;
  profiles;
  designations;
  currencies;
  contacts;
  ugqualifications;
  pgqualifications;
  doctoratequalifications;
  qualifications;
  contactpersons;
  
  userid = localStorage.getItem('id');


  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get("id");

  	this.apiService.getUserInfo(this.userid).subscribe((data:any)=>{
  	   //console.log(data);
  		this.user = data;
  	});


	this.apiService.getJobDetail(this.id).subscribe((data:any)=>{
      console.log(data);
      this.job = data;

    });

    this.apiService.getJobContacts(this.id).subscribe((data:any)=>{
     // console.log(data);
     this.contacts = data.contacts;
    });


    this.apiService.getJobLocations(this.id).subscribe((data:any)=>{
      //console.log(data);
      var q = '';
      this.countries = '';
      for(var i=0;i<data.jobcountries.length;i++)
      {
        this.countries += q+data.jobcountries[i].name;
        var q = ',';
      }
    });

    this.apiService.getJobRoles(this.id).subscribe((data:any)=>{
      //console.log(data);
       var q = '';
       this.industries = '';
      for(var i=0;i<data.jobindustries.length;i++)
      {
        this.industries += q+data.jobindustries[i].Title;
        var q = ',';
      }

       var q = '';
       this.profiles = '';
      for(var i=0;i<data.jobprofiles.length;i++)
      {
        this.profiles += q+data.jobprofiles[i].Title;
        var q = ',';
      }

       var q = '';
       this.designations = '';
      for(var i=0;i<data.jobdesignations.length;i++)
      {
        this.designations += q+data.jobdesignations[i].Title;
        var q = ',';
      }
    });


      this.apiService.getJobQualifications(this.id).subscribe((data:any)=>{
      //console.log(data);

      var q = '';
      this.qualifications = '';
      for(var i=0;i<data.jobugqualifications.length;i++)
      {
        this.qualifications += q+data.jobugqualifications[i].Title;
        var q = ',';
      }

      for(var i=0;i<data.jobpgqualifications.length;i++)
      {
        this.qualifications += q+data.jobpgqualifications[i].Title;
        var q = ',';
      }

      for(var i=0;i<data.jobdoctoratequalifications.length;i++)
      {
        this.qualifications += q+data.jobdoctoratequalifications[i].Title;
        var q = ',';
      }

    });

  }
  

}
