import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  p: number = 1;
  jobs;
  countries;
  profiles;
  submitted = false;
  error;
  years = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
 // selected = ['1'];
  filterForm = this.fb.group({
    countries: [[]],
    profiles: [[]],
    skill: [''],
    experience:[''],
    ctc:['']
  });

  constructor(private fb: FormBuilder,private apiService: ApiService,private router: Router) { }

  ngOnInit() {



    if(sessionStorage.getItem('filter') == 'Yes')
    {

    
       this.filterForm.controls.countries.setValue(sessionStorage.getItem('countries').split(','));
       this.filterForm.controls.profiles.setValue(sessionStorage.getItem('profiles').split(','));
       this.filterForm.controls.skill.setValue(sessionStorage.getItem('skill'));
       this.filterForm.controls.experience.setValue(sessionStorage.getItem('experience'));
       this.filterForm.controls.ctc.setValue(sessionStorage.getItem('ctc'));

       sessionStorage.removeItem("filter");
       sessionStorage.removeItem("countries");
       sessionStorage.removeItem("profiles");
       sessionStorage.removeItem("skill");
       sessionStorage.removeItem("experience");
       sessionStorage.removeItem("ctc");

       this.onSubmit();
    }
    else
    {
      this.apiService.getAllJobs().subscribe((data:any)=>{
        //console.log(data);
        this.jobs = data;
      });

    }


    this.apiService.getCountries().subscribe((data)=>{
       this.countries = data;
    });

    this.apiService.getProfiles().subscribe((data)=>{
       this.profiles = data;
    });
  }

  onSubmit()
  {
     this.apiService.filterJobs(this.filterForm.value).subscribe((data:any)=>{
      this.jobs = data;
    });

  }

  Reset()
  {
     
     this.filterForm.controls.countries.setValue('');
     this.filterForm.controls.profiles.setValue('');
     this.filterForm.controls.skill.setValue('');
     this.filterForm.controls.experience.setValue('');
     this.filterForm.controls.ctc.setValue('');

     this.onSubmit();
  }

}
