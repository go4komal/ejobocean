import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-matched-jobs',
  templateUrl: './matched-jobs.component.html',
  styleUrls: ['./matched-jobs.component.css']
})
export class MatchedJobsComponent implements OnInit {
   
    p: number = 1;
  jobs;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  	this.apiService.getMatchedJobs(this.userid).subscribe((data)=>{
       //console.log(data);
       this.jobs = data;
    });

  }

  

}
