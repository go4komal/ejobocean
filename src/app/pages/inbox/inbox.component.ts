import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
 
  p: number = 1;
  mails;
   userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

    this.apiService.inbox(this.userid).subscribe((data)=>{
      //console.log(data);
       this.mails = data;
    });
  }

   getUserType()
  {
    return localStorage.getItem('usertype');
  }

}
