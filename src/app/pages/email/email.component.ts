import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  
   id;
   email;
    userid = localStorage.getItem('id');
  constructor(private apiService: ApiService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {

  	this.id = this.route.snapshot.paramMap.get("id");

  	this.apiService.mail(this.id).subscribe((data:any)=>{
     // console.log(data);
      this.email = data;

    });
  }

   getUserType()
  {
    return localStorage.getItem('usertype');
  }


}
