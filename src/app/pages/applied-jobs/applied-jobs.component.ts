import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-applied-jobs',
  templateUrl: './applied-jobs.component.html',
  styleUrls: ['./applied-jobs.component.css']
})
export class AppliedJobsComponent implements OnInit {
   
    p: number = 1;
  jobs;
  userid = localStorage.getItem('id');
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  	this.apiService.getAppliedJobs(this.userid,'jobseeker').subscribe((data)=>{
       //console.log(data);
       this.jobs = data;
    });

  }

}
