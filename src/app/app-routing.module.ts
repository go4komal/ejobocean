import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegisterEmployerComponent } from './pages/register-employer/register-employer.component';
import { LoginComponent } from './pages/login/login.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { BlogComponent } from './pages/blog/blog.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { JobsComponent } from './pages/jobs/jobs.component';
import { GovernmentJobsComponent } from './pages/government-jobs/government-jobs.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { TermsConditionComponent } from './pages/terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { FaqsComponent } from './pages/faqs/faqs.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { JobDetailComponent } from './pages/job-detail/job-detail.component';
import { FileuploadComponent } from './pages/fileupload/fileupload.component';
import { EmployerProfileComponent } from './pages/employer-profile/employer-profile.component';
import { ContactPersonsComponent } from './pages/contact-persons/contact-persons.component';
import { EmployersDashboardComponent } from './pages/employers-dashboard/employers-dashboard.component';
import { WorkExperienceComponent } from './pages/work-experience/work-experience.component';
import { EducationComponent } from './pages/education/education.component';
import { PreferrencesComponent } from './pages/preferrences/preferrences.component';
import { OtherDetailsComponent } from './pages/other-details/other-details.component';
import { LanguagesComponent } from './pages/languages/languages.component';
import { PostJobComponent } from './pages/post-job/post-job.component';
import { SavedEmployersJobsComponent } from './pages/saved-employers-jobs/saved-employers-jobs.component';
import { PostedJobsComponent } from './pages/posted-jobs/posted-jobs.component';
import { EditJobComponent } from './pages/edit-job/edit-job.component';
import { ViewJobComponent } from './pages/view-job/view-job.component';
import { JobsbylocationComponent } from './pages/jobsbylocation/jobsbylocation.component';
import { JobsbyprofileComponent } from './pages/jobsbyprofile/jobsbyprofile.component';
import { SavedJobsComponent } from './pages/saved-jobs/saved-jobs.component';
import { AppliedJobsComponent } from './pages/applied-jobs/applied-jobs.component';
import { JobApplicationsComponent } from './pages/job-applications/job-applications.component';
import { JobsbyemployerComponent } from './pages/jobsbyemployer/jobsbyemployer.component';
import { JobsbydateComponent } from './pages/jobsbydate/jobsbydate.component';
import { MatchedJobsComponent } from './pages/matched-jobs/matched-jobs.component';
import { PrivacySettingsComponent } from './pages/privacy-settings/privacy-settings.component';
import { BlockCompaniesComponent } from './pages/block-companies/block-companies.component';
import { VerifyEmailComponent } from './pages/verify-email/verify-email.component';
import { EmailVerificationComponent } from './pages/email-verification/email-verification.component';
import { ManageLettersComponent } from './pages/manage-letters/manage-letters.component';
import { SendEmailComponent } from './pages/send-email/send-email.component';
import { InboxComponent } from './pages/inbox/inbox.component';
import { OutboxComponent } from './pages/outbox/outbox.component';
import { EmailComponent } from './pages/email/email.component';
import { ResumeSearchComponent } from './pages/resume-search/resume-search.component';
import { SavedResumesComponent } from './pages/saved-resumes/saved-resumes.component';
import { JobseekerProfileComponent } from './pages/jobseeker-profile/jobseeker-profile.component';
import { LoginWithLinkedinComponent } from './pages/login-with-linkedin/login-with-linkedin.component';
import { InterviewerComponent } from './pages/interviewer/interviewer.component';

const routes: Routes = [
{
	path: '',
	component: HomeComponent,
	data: { title: 'Home' }
},
{
	path: 'home',
	component: HomeComponent,
	data: { title: 'Home' }
},
{
	path: 'jobs',
	component: JobsComponent,
	data: { title: 'Jobs' }
},
{
	path: 'government-jobs',
	component: GovernmentJobsComponent,
	data: { title: 'Government Jobs' }
},
{
	path: 'faqs',
	component: FaqsComponent,
	data: { title: 'FAQs' }
},
{
	path: 'blogs',
	component: BlogsComponent,
	data: { title: 'Blogs' }
},
{
	path: 'blog/:id',
	component: BlogComponent,
	data: { title: 'Blog' }
},
{
	path: 'job-detail/:id',
	component: JobDetailComponent,
	data: { title: 'Job Detail' }
},
{
	path: 'interviewer/:id',
	component: InterviewerComponent,
	data: { title: 'Interviewer' }
},
{
	path: 'verify-email',
	component: VerifyEmailComponent,
	data: { title: 'Verify Email' }
},
{
	path: 'email-verification/:code',
	component: EmailVerificationComponent,
	data: { title: 'Email Verification' }
},
{
	path: 'about-us',
	component: AboutUsComponent,
	data: { title: 'About Us' }
},
{
	path: 'login',
	component: LoginComponent,
	data: { title: 'Login' }
},
{
	path: 'register',
	component: RegisterComponent,
	data: { title: 'Register Jobseeker' }
},
{
	path: 'register-employer',
	component: RegisterEmployerComponent,
	data: { title: 'Register Employer' }
},
{
	path: 'terms-condition',
	component: TermsConditionComponent,
	data: { title: 'Terms and conditions' }
},
{
	path: 'privacy-policy',
	component: PrivacyPolicyComponent,
	data: { title: 'Privacy Policy' }
},
{
	path: 'contact-us',
	component: ContactUsComponent,
	data: { title: 'Contact Us' }
},
{
	path: 'dashboard',
	component: DashboardComponent,
	data: { title: 'Dashboard' }
},
{
	path: 'profile',
	component: ProfileComponent,
	data: { title: 'Profile' }
},
{
	path: 'employer-profile',
	component: EmployerProfileComponent,
	data: { title: 'Employer Profile' }
},
{
	path: 'change-password',
	component: ChangePasswordComponent,
	data: { title: 'Change Password' }
},
{
	path: 'forgot-password',
	component: ForgotPasswordComponent,
	data: { title: 'Forgot Password' }
},
{
	path: 'reset-password/:code',
	component: ResetPasswordComponent,
	data: { title: 'Reset Password' }
},
{
	path: 'fileupload',
	component: FileuploadComponent,
	data: { title: 'File Upload' }
},
{
	path: 'contact-persons',
	component: ContactPersonsComponent,
	data: { title: 'Contact Persons' }
},
{
	path: 'employers-dashboard',
	component: EmployersDashboardComponent,
	data: { title: 'Employers Dashboard' }
},
{
	path: 'work-experience',
	component: WorkExperienceComponent,
	data: { title: 'Work Experience' }
},
{
	path: 'education',
	component: EducationComponent,
	data: { title: 'Education' }
},
{
	path: 'preferrences',
	component: PreferrencesComponent,
	data: { title: 'Preferrences' }
},
{
	path: 'languages',
	component: LanguagesComponent,
	data: { title: 'Langauges' }
},
{
	path: 'other-details',
	component: OtherDetailsComponent,
	data: { title: 'Other details' }
},
{
	path: 'post-job',
	component: PostJobComponent,
	data: { title: 'Post Job' }
},
{
	path: 'saved-employers-jobs',
	component: SavedEmployersJobsComponent,
	data: { title: 'Saved Employer Jobs' }
},
{
	path: 'posted-jobs',
	component: PostedJobsComponent,
	data: { title: 'Posted Jobs' }
},
{
	path: 'saved-jobs',
	component: SavedJobsComponent,
	data: { title: 'Saved Jobs' }
},
{
	path: 'matched-jobs',
	component: MatchedJobsComponent,
	data: { title: 'Matched Jobs' }
},
{
	path: 'applied-jobs',
	component: AppliedJobsComponent,
	data: { title: 'Applied Jobs' }
},
{
	path: 'job-applications',
	component: JobApplicationsComponent,
	data: { title: 'Job Applications' }
},
{
	path: 'edit-job/:id',
	component: EditJobComponent,
	data: { title: 'Edit Job' }
},
{
	path: 'view-job/:id',
	component: ViewJobComponent,
	data: { title: 'View Job' }
},
{
	path: 'jobsbyprofile/:id',
	component: JobsbyprofileComponent,
	data: { title: 'Jobs By Profile' }
},
{
	path: 'jobsbylocation/:id',
	component: JobsbylocationComponent,
	data: { title: 'Jobs By Location' }
},
{
	path: 'jobsbydate/:id',
	component: JobsbydateComponent,
	data: { title: 'Jobs By Location' }
},
{
	path: 'jobsbyemployer/:id',
	component: JobsbyemployerComponent,
	data: { title: 'Jobs By Location' }
},
{
	path: 'privacy-settings',
	component: PrivacySettingsComponent,
	data: { title: 'Privacy Settings' }
},
{
	path: 'block-companies',
	component: BlockCompaniesComponent,
	data: { title: 'Block Companies' }
},
{
	path: 'manage-letters',
	component: ManageLettersComponent,
	data: { title: 'Manage Letters' }
},
{
	path: 'send-email',
	component: SendEmailComponent,
	data: { title: 'Send Email' }
},
{
	path: 'inbox',
	component: InboxComponent,
	data: { title: 'Inbox' }
},
{
	path: 'outbox',
	component: OutboxComponent,
	data: { title: 'Outbox' }
},
{
	path: 'email/:id',
	component: EmailComponent,
	data: { title: 'Email' }
},
{
	path: 'resume-search',
	component: ResumeSearchComponent,
	data: { title: 'Resume Search' }
},
{
	path: 'saved-resumes',
	component: SavedResumesComponent,
	data: { title: 'Saved Resumes' }
},
{
	path: 'jobseeker-profile/:id',
	component: JobseekerProfileComponent,
	data: { title: 'Jobseeker Profile' }
},
{
	path: 'login-with-linkedin',
	component: LoginWithLinkedinComponent,
	data: { title: 'Login With LinkedIn' }
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
